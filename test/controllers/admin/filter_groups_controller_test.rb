require 'test_helper'

class Admin::FilterGroupsControllerTest < ActionController::TestCase
  setup do
    @filter_group = filter_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:filter_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create filter_group" do
    assert_difference('FilterGroup.count') do
      post :create, filter_group: { desc: @filter_group.desc, mid: @filter_group.mid, model: @filter_group.model, name: @filter_group.name, position: @filter_group.position, public: @filter_group.public }
    end

    assert_redirected_to admin_filter_group_path(assigns(:filter_group))
  end

  test "should show filter_group" do
    get :show, id: @filter_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @filter_group
    assert_response :success
  end

  test "should update filter_group" do
    patch :update, id: @filter_group, filter_group: { desc: @filter_group.desc, mid: @filter_group.mid, model: @filter_group.model, name: @filter_group.name, position: @filter_group.position, public: @filter_group.public }
    assert_redirected_to admin_filter_group_path(assigns(:filter_group))
  end

  test "should destroy filter_group" do
    assert_difference('FilterGroup.count', -1) do
      delete :destroy, id: @filter_group
    end

    assert_redirected_to admin_filter_groups_path
  end
end
