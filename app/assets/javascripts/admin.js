//= require jquery
//= underscore-min
//= require jquery_ujs
//= require html.sortable
//= require_tree ./admin
//= require ckeditor/init
//= require codemirror
//= require codemirror/modes/xml
//= require codemirror/modes/htmlmixed
//= require codemirror/modes/javascript
//= require codemirror/modes/ruby


var tready, treadys, sortableBlock, set_positions, nestedTreeSorter;

set_positions = function () {
    // loop through and give each task a data-pos
    // attribute that holds its position in the DOM
    $('.panel.panel-default').each(function (i) {
        $(this).attr("data-pos", i + 1);
    });
}

tready = function () {

    // call set_positions function
    set_positions();

    $('.sortable').sortable();

    // after the order changes
    $('.sortable').sortable().bind('sortupdate', function (e, ui) {
        // array to store new order
        updated_order = []
        // set the updated positions
        set_positions();

        // populate the updated_order array with the new fields positions
        $('.fci').each(function (i) {
            updated_order.push({id: $(this).data("id"), position: i + 1});
        });

        // send the updated order via ajax
        $.ajax({
            type: "PUT",
            url: '/admin/fcis/sort',
            data: {order: updated_order}
        });
    });
}
treadys = function () {

    // call set_positions function
    set_positions();

    $('.sortabled').sortable();

    // after the order changes
    $('.sortabled').sortable().bind('sortupdate', function (e, ui) {
        // array to store new order
        updated_order = []
        // set the updated positions
        set_positions();

        // populate the updated_order array with the new fields positions
        $('.status').each(function (i) {
            updated_order.push({id: $(this).data("id"), position: i + 1});
        });

        // send the updated order via ajax
        $.ajax({
            type: "PUT",
            url: '/admin/status_orders/sort',
            data: {order: updated_order}
        });
    });
}
nestedTreeSorter = function (e, uri) {
    return function () {
        $(e).nestedSortable({
            listType: 'ul',
            opacity: 0.6,
            handle: '.handle',
            items: 'li',
            toleranceElement: '> span',
            update: function (event, ui) {
                var mylist = $(e).sortable('serialize');
                var mylist = mylist.replace(/root/g, '');
                var finalstring = '';
                $(mylist.split("&")).each(function (index) {
                    string2 = this.split("[");
                    type = string2[0];
                    string3 = string2[1].split("]");
                    id = string3[0];
                    fragments = this.split('=');
                    var parent_id = fragments[1];
                    finalstring = finalstring + type + '[' + index + ']' + '[id]=' + id + '&' + type + '[' + index + '][parent_id]=' + parent_id + '&' + type + '[' + index + '][position]=' + index + '&';
                });
                $.post(uri, finalstring);
            }
        });
    }
}

sortableBlock = function () {
    $(".sortable-block").nestedSortable({
        disableNestingClass: 'no-nest',
        listType: 'ul',
        opacity: 0.6,
        handle: '.handle',
        items: "li:not(.dontsortme)",
        toleranceElement: '> div',
        connectWith: '.sortable-block',
        update: function (event, ui) {
            var finalstring = '';
            $('.no-nest').each(function (i) {
                if ($(this).data("id")) {
                    var id = $(this).data("id");
                    var region = $(this).parent().data('id');
                    console.log($(this).parent().data('id'));
                    finalstring = finalstring + 'blocks' + '[' + i + ']' + '[id]=' + id + '&' + 'blocks' + '[' + i + '][region]=' + region + '&' + 'blocks' + '[' + i + '][position]=' + i + '&';
                }
            });
            $.post("/admin/blocks/sort", finalstring);
        }
    });
}

$(document).ready(nestedTreeSorter("#sortable-taxonomy", "/admin/taxonomies/sort"));
$(document).ready(nestedTreeSorter("#sortable-list", "/admin/menu_items/sort"));
$(document).ready(tready);
$(document).ready(treadys);
$(document).ready(sortableBlock);
/**
 * if using turbolinks
 */
$(document).on('page:load', tready);
