class Admin::PluginManagersController < Admin::BaseController
  before_action :set_plugin_manager, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  load_and_authorize_resource
  # GET /admin/plugin_managers
  # GET /admin/plugin_managers.json
  def index
    @plugin_managers = PluginManager.all
  end

  # GET /admin/plugin_managers/1
  # GET /admin/plugin_managers/1.json
  def show
  end

  # GET /admin/plugin_managers/new
  def new
    @plugin_manager = PluginManager.new
  end

  # GET /admin/plugin_managers/1/edit
  def edit
  end

  # POST /admin/plugin_managers
  # POST /admin/plugin_managers.json
  def create
    @plugin_manager = PluginManager.new(plugin_manager_params)

    respond_to do |format|
      if @plugin_manager.save
        format.html { redirect_to [:admin, @plugin_manager], notice: 'Plugin manager was successfully created.' }
        format.json { render action: 'show', status: :created, location: @plugin_manager }
      else
        format.html { render action: 'new' }
        format.json { render json: @plugin_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/plugin_managers/1
  # PATCH/PUT /admin/plugin_managers/1.json
  def update
    respond_to do |format|
      if @plugin_manager.update(plugin_manager_params)
        format.html { redirect_to [:admin, @plugin_manager], notice: 'Plugin manager was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @plugin_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/plugin_managers/1
  # DELETE /admin/plugin_managers/1.json
  def destroy
    @plugin_manager.destroy
    respond_to do |format|
      format.html { redirect_to admin_plugin_managers_url, notice: 'Plugin manager was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_plugin_manager
    @plugin_manager = PluginManager.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def plugin_manager_params
    params[:plugin_manager]
  end
end
