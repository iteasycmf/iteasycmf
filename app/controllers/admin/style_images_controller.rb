class Admin::StyleImagesController < Admin::BaseController
  before_action :set_style_image, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  load_and_authorize_resource
  # GET /admin/style_images
  # GET /admin/style_images.json
  def index
    @style_images = StyleImage.all
  end

  # GET /admin/style_images/1
  # GET /admin/style_images/1.json
  def show
  end

  # GET /admin/style_images/new
  def new
    @style_image = StyleImage.new
  end

  # GET /admin/style_images/1/edit
  def edit
  end

  # POST /admin/style_images
  # POST /admin/style_images.json
  def create
    @style_image = StyleImage.new(style_image_params)

    respond_to do |format|
      if @style_image.save
        format.html { redirect_to [:admin, @style_image], notice: 'Style image was successfully created.' }
        format.json { render action: 'show', status: :created, location: @style_image }
      else
        format.html { render action: 'new' }
        format.json { render json: @style_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/style_images/1
  # PATCH/PUT /admin/style_images/1.json
  def update
    respond_to do |format|
      if @style_image.update(style_image_params)
        format.html { redirect_to [:admin, @style_image], notice: 'Style image was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @style_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/style_images/1
  # DELETE /admin/style_images/1.json
  def destroy
    @style_image.destroy
    respond_to do |format|
      format.html { redirect_to admin_style_images_url, notice: 'Style image was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_style_image
    @style_image = StyleImage.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def style_image_params
    params.require(:style_image).permit(:name, :machine_name, :process, :widht, :height, :percent)
  end
end
