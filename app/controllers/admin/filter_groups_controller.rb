class Admin::FilterGroupsController < ApplicationController
  before_action :set_filter_group, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  load_and_authorize_resource
  # GET /admin/filter_groups
  # GET /admin/filter_groups.json
  def index
    @filter_groups = FilterGroup.all
  end

  # GET /admin/filter_groups/1
  # GET /admin/filter_groups/1.json
  def show
  end

  # GET /admin/filter_groups/new
  def new
    @filter_group = FilterGroup.new
  end

  # GET /admin/filter_groups/1/edit
  def edit
  end

  # POST /admin/filter_groups
  # POST /admin/filter_groups.json
  def create
    @filter_group = FilterGroup.new(filter_group_params)

    respond_to do |format|
      if @filter_group.save
        format.html { redirect_to [:admin, @filter_group], notice: 'Filter group was successfully created.' }
        format.json { render action: 'show', status: :created, location: @filter_group }
      else
        format.html { render action: 'new' }
        format.json { render json: @filter_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/filter_groups/1
  # PATCH/PUT /admin/filter_groups/1.json
  def update
    respond_to do |format|
      if @filter_group.update(filter_group_params)
        format.html { redirect_to [:admin, @filter_group], notice: 'Filter group was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @filter_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/filter_groups/1
  # DELETE /admin/filter_groups/1.json
  def destroy
    @filter_group.destroy
    respond_to do |format|
      format.html { redirect_to admin_filter_groups_url, notice: 'Filter group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_filter_group
      @filter_group = FilterGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def filter_group_params
      params.require(:filter_group).permit(:name, :public, :position, :mid, :model, :desc)
    end
end
