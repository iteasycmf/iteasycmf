class Admin::SystemsController < Admin::BaseController
  before_action :set_system, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  load_and_authorize_resource
  # GET /admin/systems
  # GET /admin/systems.json
  def index
    @systems = System.where(hidden: false  || nil)
  end

  # GET /admin/systems/1
  # GET /admin/systems/1.json
  def show
  end

  # GET /admin/systems/new
  def new
    @system = System.new
  end

  # GET /admin/systems/1/edit
  def edit
  end

  # POST /admin/systems
  # POST /admin/systems.json
  def create
    @system = System.new(system_params)

    respond_to do |format|
      if @system.save
        format.html { redirect_to [:admin, @system], notice: 'System was successfully created.' }
        format.json { render action: 'show', status: :created, location: @system }
      else
        format.html { render action: 'new' }
        format.json { render json: @system.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/systems/1
  # PATCH/PUT /admin/systems/1.json
  def update
    respond_to do |format|
      if @system.update(system_params)
        format.html { redirect_to :back, notice: 'System was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @system.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/systems/1
  # DELETE /admin/systems/1.json
  def destroy
    @system.destroy
    respond_to do |format|
      format.html { redirect_to admin_systems_url, notice: 'System was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_system
    @system = System.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def system_params
    params.require(:system).permit(:name, :value, :valueb, :image, :help, :category, :key, :machine_category, :blocked,
                                   :hidden, :position)
  end
end
