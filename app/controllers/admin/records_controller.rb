class Admin::RecordsController < Admin::BaseController
  skip_before_filter :verify_authenticity_token
  before_action :set_record, only: [:show, :edit, :update, :destroy, :taxonomypage, :relationpage]
  layout 'admin'
  load_and_authorize_resource
  respond_to :js
  # GET /admin/records
  # GET /admin/records.json
  def index
    @record_type_records = RecordType.where(disabled: false, model: 'Record')
    breadcrumbs.add t('listing records'), admin_records_url
  end

  def list
    @record_type = RecordType.find_by(model: 'Record', id: params[:record_type_id])
    @q = Record.where(record_type_id: params[:record_type_id]).ransack(params[:q])
    @records = @q.result.page params[:page]
    @cell = Cell.where(record_type_id: 9, public: true).search(params[:q])
    @celllists = @cell.result.uniq.page params[:page]
    breadcrumbs.add t('listing records'), admin_records_url
    breadcrumbs.add @record_type.name
  end

  # GET /admin/records/1
  # GET /admin/records/1.json
  def show
    @search = RecordType.search(params[:q])
    @types = @search.result.page params[:page]
    @mr=ModelRelation.new
    @cell= Cell.new
    @comment = Comment.new
    breadcrumbs.add t('listing records'), admin_records_url
    breadcrumbs.add @record.record_type.name, admin_records_list_url(@record.record_type_id)
    breadcrumbs.add @record.title
  end

  # GET /admin/records/new
  def new
    @record = Record.new
    breadcrumbs.add t('listing records'), admin_records_url
    @record_type = RecordType.find(params[:record_type_id])
  end

  # GET /admin/records/1/edit
  def edit
    breadcrumbs.add t('listing records'), admin_records_url
    breadcrumbs.add @record.record_type.name, admin_records_list_url(@record.record_type_id)
    breadcrumbs.add t('edit')+' '+@record.title unless @record.title.nil?
  end

  # POST /admin/records
  # POST /admin/records.json
  def create
    @record = Record.new(record_params)
    respond_to do |format|
      if @record.save
        @seomore=Seomore.create
        @record.update(seomore_id: @seomore.id)
        instance_saver(@record, params)
        format.html { redirect_to edit_admin_record_path(@record), notice: 'Record was successfully created.' }
        format.json { render action: 'show', status: :created, location: @record }
      else
        format.html { render action: 'new' }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  def taxonomypage
    breadcrumbs.add t('listing records'), admin_records_url
    breadcrumbs.add @record.record_type.name, admin_records_list_url(@record.record_type_id)
    breadcrumbs.add t('edit')+' '+@record.title
    @searchtax = Taxonomy.all.roots.search(params[:q])
    if params[:q].nil?
      @taxonomies = Taxonomy.where(record_type_id: RecordType.find_by(model: 'Taxonomy').id).roots.page params[:page]
    else
      @taxonomies = @searchtax.result.page params[:page]
    end
    @mr=ModelRelation.new
  end

  def relationpage
    breadcrumbs.add t('listing records'), admin_records_url
    breadcrumbs.add @record.record_type.name, admin_records_list_url(@record.record_type_id)
    breadcrumbs.add t('edit')+' '+@record.title
    @cell= Cell.new
    @model=@record
    @searchcell = @record.cells.search(params[:q])
    if params[:q].nil?
      @cells = @record.cells.where(record_type_id: RecordType.find_by(model: 'Cell').id).page params[:page]
    else
      @cells = @searchcell.result.page params[:page]
    end
  end

  # PATCH/PUT /admin/records/1
  # PATCH/PUT /admin/records/1.json
  def update
    seomore = @record.build_seomore

    respond_to do |format|
      if @record.update(record_params)
        instance_saver(@record, params)
        format.html { redirect_to edit_admin_record_url(@record), notice: 'Record was successfully updated.' }
        format.json { head :no_content }
        format.js {}
      else
        format.html { render action: 'edit' }
        format.json { render json: @record.errors, status: :unprocessable_entity }
        format.js {}
      end
    end
  end

  # DELETE /admin/records/1
  # DELETE /admin/records/1.json
  def destroy
    @record.destroy
    respond_to do |format|
      format.html { redirect_to admin_records_url, notice: 'Record was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_record
    @record = Record.friendly.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def record_params
    params.require(:record).permit(:machine_name, :record_type_id, :url, :public, :sticky,
                                   :seomore_id, :menu_item_id, seomore_attributes: [:title, :keywords, :desc, :noindex, :nofollow, :canonical, :author, :publisher, :alternate, :refresh],
                                   fci_images_attributes: [:value, :alt, :title, :fci, :mid, :model, :remote_image_url])
  end
end
