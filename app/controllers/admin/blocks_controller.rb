class Admin::BlocksController < Admin::BaseController
  before_action :set_block, only: [:show, :edit, :update, :destroy, :relationpage]
  layout 'admin'
  load_and_authorize_resource
  skip_before_filter :verify_authenticity_token
  respond_to :js
  # GET /admin/blocks
  # GET /admin/blocks.json
  def index
    @blocks = Block.all
    breadcrumbs.add t('listing blocks'), admin_blocks_url
    @block = Block.new
  end

  # GET /admin/blocks/1
  # GET /admin/blocks/1.json
  def show

  end

  # GET /admin/blocks/new
  def new
    @block = Block.new
  end

  # GET /admin/blocks/1/edit
  def edit
    breadcrumbs.add t('listing blocks'), admin_blocks_url
    breadcrumbs.add t('editing block'), edit_admin_block_url
  end

  # POST /admin/blocks
  # POST /admin/blocks.json
  def create
    @block = Block.new(block_params)

    respond_to do |format|
      if @block.save
        format.html { redirect_to edit_admin_block_path(@block), notice: 'Block was successfully created.' }
        format.json { render action: 'show', status: :created, location: @block }
      else
        format.html { render action: 'new' }
        format.json { render json: @block.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/blocks/1
  # PATCH/PUT /admin/blocks/1.json
  def update
    respond_to do |format|
      if @block.update(block_params)
        instance_saver(@block, params)
        format.html { redirect_to admin_blocks_url, notice: 'Block was successfully updated.' }
        format.json { respond_with_bip(@block) }
      else
        format.html { render action: 'edit' }
        format.json { render json: @block.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/blocks/1
  # DELETE /admin/blocks/1.json
  def destroy
    @block.destroy
    respond_to do |format|
      format.html { redirect_to admin_blocks_url, notice: 'Block was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def relationpage
    @cell= Cell.new
    @model=@block
    @searchcell = @block.cells.search(params[:q])
    if params[:q].nil?
      @cells = @block.cells.where(record_type_id: RecordType.find_by(model: 'Cell').id).page params[:page]
    else
      @cells = @searchcell.result.page params[:page]
    end
  end

  def sort
    params[:blocks].each do |key, value|
      Block.find(value[:id]).update_attributes(position: value[:position], region: value[:region])
    end
    render :nothing => true
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_block
    @block = Block.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def block_params
    params.require(:block).permit(:name, :region, :type_url, :url, :body, :csscls, :machine_name, :public, :position,
                                  :system, :record_type_id)
  end
end