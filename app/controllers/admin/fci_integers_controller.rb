class Admin::FciIntegersController < ApplicationController
  before_action :set_fci_integer, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  # GET /admin/fci_integers
  # GET /admin/fci_integers.json
  def index
    @fci_integers = FciInteger.all
  end

  # GET /admin/fci_integers/1
  # GET /admin/fci_integers/1.json
  def show
  end

  # GET /admin/fci_integers/new
  def new
    @fci_integer = FciInteger.new
  end

  # GET /admin/fci_integers/1/edit
  def edit
  end

  # POST /admin/fci_integers
  # POST /admin/fci_integers.json
  def create
    @fci_integer = FciInteger.new(fci_integer_params)

    respond_to do |format|
      if @fci_integer.save
        format.html { redirect_to [:admin, @fci_integer], notice: 'Fci integer was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fci_integer }
      else
        format.html { render action: 'new' }
        format.json { render json: @fci_integer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/fci_integers/1
  # PATCH/PUT /admin/fci_integers/1.json
  def update
    respond_to do |format|
      if @fci_integer.update(fci_integer_params)
        format.html { redirect_to [:admin, @fci_integer], notice: 'Fci integer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fci_integer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/fci_integers/1
  # DELETE /admin/fci_integers/1.json
  def destroy
    @fci_integer.destroy
    respond_to do |format|
      format.html { redirect_to admin_fci_integers_url, notice: 'Fci integer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_integer
    @fci_integer = FciInteger.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fci_integer_params
    params.require(:fci_integer).permit(:value, :fci_id, :mid, :model)
  end
end
