class Admin::FciImagesController < Admin::BaseController
  before_action :set_fci_image, only: [:show, :edit, :update, :destroy]

  # GET /admin/fci_images
  # GET /admin/fci_images.json
  def index
    @fci_images = FciImage.all
  end

  # GET /admin/fci_images/1
  # GET /admin/fci_images/1.json
  def show
  end

  # GET /admin/fci_images/new
  def new
    @fci_image = FciImage.new
  end

  # GET /admin/fci_images/1/edit
  def edit
  end

  # POST /admin/fci_images
  # POST /admin/fci_images.json
  def create
    @fci_image = FciImage.new(fci_image_params)

    respond_to do |format|
      if @fci_image.save
        format.html { redirect_to [:admin, @fci_image], notice: 'Fci image was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fci_image }
      else
        format.html { render action: 'new' }
        format.json { render json: @fci_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/fci_images/1
  # PATCH/PUT /admin/fci_images/1.json
  def update
    respond_to do |format|
      if @fci_image.update(fci_image_params)
        format.html { redirect_to [:admin, @fci_image], notice: 'Fci image was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fci_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/fci_images/1
  # DELETE /admin/fci_images/1.json
  def destroy
    @fci_image.destroy
    respond_to do |format|
      format.html { redirect_to :back, :gflash => {:success => 'Вы удалили изображение'} }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_image
    @fci_image = FciImage.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fci_image_params
    params.require(:fci_image).permit(:value, :alt, :title, :fci, :mid, :model, :remote_image_url)
  end
end
