class Admin::BaseController < ApplicationController
  #theme :pacifonic_admin
  #force_ssl

  def current_ability
    @current_ability ||= AdminAbility.new(current_user)
  end


end