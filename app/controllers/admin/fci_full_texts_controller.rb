class Admin::FciFullTextsController < ApplicationController
  before_action :set_fci_full_text, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  # GET /admin/fci_full_texts
  # GET /admin/fci_full_texts.json
  def index
    @fci_full_texts = FciFullText.all
  end

  # GET /admin/fci_full_texts/1
  # GET /admin/fci_full_texts/1.json
  def show
  end

  # GET /admin/fci_full_texts/new
  def new
    @fci_full_text = FciFullText.new
  end

  # GET /admin/fci_full_texts/1/edit
  def edit
  end

  # POST /admin/fci_full_texts
  # POST /admin/fci_full_texts.json
  def create
    @fci_full_text = FciFullText.new(fci_full_text_params)

    respond_to do |format|
      if @fci_full_text.save
        format.html { redirect_to [:admin, @fci_full_text], notice: 'Fci full text was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fci_full_text }
      else
        format.html { render action: 'new' }
        format.json { render json: @fci_full_text.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/fci_full_texts/1
  # PATCH/PUT /admin/fci_full_texts/1.json
  def update
    respond_to do |format|
      if @fci_full_text.update(fci_full_text_params)
        format.html { redirect_to [:admin, @fci_full_text], notice: 'Fci full text was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fci_full_text.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/fci_full_texts/1
  # DELETE /admin/fci_full_texts/1.json
  def destroy
    @fci_full_text.destroy
    respond_to do |format|
      format.html { redirect_to admin_fci_full_texts_url, notice: 'Fci full text was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_full_text
    @fci_full_text = FciFullText.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fci_full_text_params
    params.require(:fci_full_text).permit(:fci_id, :mid, :model, :value, :format_field)
  end
end
