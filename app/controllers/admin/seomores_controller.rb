class Admin::SeomoresController < Admin::BaseController
  before_action :set_seomore, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  load_and_authorize_resource
  # GET /admin/seomores
  # GET /admin/seomores.json
  def index
    @seomores = Seomore.all
  end

  # GET /admin/seomores/1
  # GET /admin/seomores/1.json
  def show
  end

  # GET /admin/seomores/new
  def new
    @seomore = Seomore.new
  end

  # GET /admin/seomores/1/edit
  def edit
  end

  # POST /admin/seomores
  # POST /admin/seomores.json
  def create
    @seomore = Seomore.new(seomore_params)

    respond_to do |format|
      if @seomore.save
        format.html { redirect_to admin_seomores_url, notice: 'Seomore was successfully created.' }
        format.json { render action: 'show', status: :created, location: @seomore }
      else
        format.html { render action: 'new' }
        format.json { render json: @seomore.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/seomores/1
  # PATCH/PUT /admin/seomores/1.json
  def update
    respond_to do |format|
      if @seomore.update(seomore_params)
        if @seomore.records.first.present?
          redirect_to admin_record_url(@seomore.records.first) and return
        else
          if @seomore.taxonomies.first.present?
            redirect_to admin_taxonomy_url(@seomore.taxonomies.first) and return
          end
        end
        format.html { redirect_to admin_seomores_url, notice: 'Seomore was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @seomore.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/seomores/1
  # DELETE /admin/seomores/1.json
  def destroy
    @seomore.destroy
    respond_to do |format|
      format.html { redirect_to admin_seomores_url, notice: 'Seomore was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_seomore
    @seomore = Seomore.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def seomore_params
    params.require(:seomore).permit(:title, :keywords, :desc, :noindex, :nofollow, :canonical, :author, :publisher, :alternate, :refresh)
  end
end
