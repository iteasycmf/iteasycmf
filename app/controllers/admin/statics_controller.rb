class Admin::StaticsController < ApplicationController
  layout 'admin'

  def dashboard
    breadcrumbs.add t('dashboard'), admin_dashboard_url
  end


end