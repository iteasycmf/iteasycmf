class Admin::FciBooleansController < Admin::BaseController
  before_action :set_fci_boolean, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  # GET /admin/fci_booleans
  # GET /admin/fci_booleans.json
  def index
    @fci_booleans = FciBoolean.all
  end

  # GET /admin/fci_booleans/1
  # GET /admin/fci_booleans/1.json
  def show
  end

  # GET /admin/fci_booleans/new
  def new
    @fci_boolean = FciBoolean.new
  end

  # GET /admin/fci_booleans/1/edit
  def edit
  end

  # POST /admin/fci_booleans
  # POST /admin/fci_booleans.json
  def create
    @fci_boolean = FciBoolean.new(fci_boolean_params)

    respond_to do |format|
      if @fci_boolean.save
        format.html { redirect_to [:admin, @fci_boolean], notice: 'Fci boolean was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fci_boolean }
      else
        format.html { render action: 'new' }
        format.json { render json: @fci_boolean.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/fci_booleans/1
  # PATCH/PUT /admin/fci_booleans/1.json
  def update
    respond_to do |format|
      if @fci_boolean.update(fci_boolean_params)
        format.html { redirect_to [:admin, @fci_boolean], notice: 'Fci boolean was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fci_boolean.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/fci_booleans/1
  # DELETE /admin/fci_booleans/1.json
  def destroy
    @fci_boolean.destroy
    respond_to do |format|
      format.html { redirect_to admin_fci_booleans_url, notice: 'Fci boolean was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_boolean
    @fci_boolean = FciBoolean.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fci_boolean_params
    params.require(:fci_boolean).permit(:value, :fci, :mid, :model)
  end
end
