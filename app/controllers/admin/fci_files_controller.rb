class Admin::FciFilesController < ApplicationController
  before_action :set_fci_file, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  # GET /admin/fci_files
  # GET /admin/fci_files.json
  def index
    @fci_files = FciFile.all
  end

  # GET /admin/fci_files/1
  # GET /admin/fci_files/1.json
  def show
  end

  # GET /admin/fci_files/new
  def new
    @fci_file = FciFile.new
  end

  # GET /admin/fci_files/1/edit
  def edit
  end

  # POST /admin/fci_files
  # POST /admin/fci_files.json
  def create
    @fci_file = FciFile.new(fci_file_params)

    respond_to do |format|
      if @fci_file.save
        format.html { redirect_to [:admin, @fci_file], notice: 'Fci file was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fci_file }
      else
        format.html { render action: 'new' }
        format.json { render json: @fci_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/fci_files/1
  # PATCH/PUT /admin/fci_files/1.json
  def update
    respond_to do |format|
      if @fci_file.update(fci_file_params)
        format.html { redirect_to [:admin, @fci_file], notice: 'Fci file was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fci_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/fci_files/1
  # DELETE /admin/fci_files/1.json
  def destroy
    @fci_file.destroy
    respond_to do |format|
      format.html { redirect_to admin_fci_files_url, notice: 'Fci file was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_file
    @fci_file = FciFile.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fci_file_params
    params.require(:fci_file).permit(:value, :fci_id, :mid, :model)
  end
end
