class Admin::TransformsController < ApplicationController
  before_action :set_transform, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  respond_to :js
  # GET /admin/transforms
  # GET /admin/transforms.json
  def index
    @search = Transform.search(params[:q])
    if params[:q].nil?
      @transforms = Transform.where(record_type_id: RecordType.find_by(model: 'Transform').id).page params[:page]
    else
      @transforms = @search.result.page params[:page]
    end
    respond_to do |format|
      format.html
      format.xls
    end
  end

  def list

  end
  # GET /admin/transforms/1
  # GET /admin/transforms/1.json
  def show
  end

  # GET /admin/transforms/new
  def new
    @transform = Transform.new
  end

  # GET /admin/transforms/1/edit
  def edit
  end

  # POST /admin/transforms
  # POST /admin/transforms.json
  def create
    @transform = Transform.new(transform_params)

    respond_to do |format|
      if @transform.save
        format.html { redirect_to [:admin, @transform], notice: 'Transform was successfully created.' }
        format.json { render action: 'show', status: :created, location: @transform }
      else
        format.html { render action: 'new' }
        format.json { render json: @transform.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/transforms/1
  # PATCH/PUT /admin/transforms/1.json
  def update
    respond_to do |format|
      if @transform.update(transform_params)
        format.html { redirect_to [:admin, @transform], notice: 'Transform was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @transform.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/transforms/1
  # DELETE /admin/transforms/1.json
  def destroy
    @transform.destroy
    respond_to do |format|
      format.html { redirect_to admin_transforms_url, notice: 'Transform was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_transform
    @transform = Transform.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def transform_params
    params.require(:transform).permit(:record_type_id, :public, :seomore_id, :menu_items_id, :block, :mainform, :calc, :url)
  end
end
