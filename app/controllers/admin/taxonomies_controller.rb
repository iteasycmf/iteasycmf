class Admin::TaxonomiesController < Admin::BaseController
  skip_before_filter :verify_authenticity_token
  before_action :set_taxonomy, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  respond_to :html, :json
  load_and_authorize_resource
  # GET /admin/taxonomies
  # GET /admin/taxonomies.json
  def index
    @record_type_taxonomies = RecordType.where(disabled: false, model: 'Taxonomy')
     breadcrumbs.add t('listing taxonomies'), admin_taxonomies_url
  end

  # GET /admin/taxonomies/1
  # GET /admin/taxonomies/1.json
  def show
    @search = RecordType.search(params[:q])
    @types = @search.result.page params[:page]
    @mr=ModelRelation.new
    @cell= Cell.new
  end

  def list
    @taxonomy = Taxonomy.new
    @record_type = RecordType.find_by(model: 'Taxonomy', id: params[:record_type_id])
    @taxonomies = Taxonomy.where(record_type_id: params[:record_type_id]).roots.page params[:page]
    breadcrumbs.add t('listing taxonomies'), admin_taxonomies_url
    breadcrumbs.add @record_type.name
  end

  # GET /admin/taxonomies/new
  def new
    @taxonomy = Taxonomy.new
  end

  # GET /admin/taxonomies/1/edit
  def edit
    breadcrumbs.add t('listing taxonomies'), admin_taxonomies_url
    breadcrumbs.add @taxonomy.record_type.name, admin_taxonomies_list_url(@taxonomy.record_type_id)
    breadcrumbs.add @taxonomy.title
  end

  # POST /admin/taxonomies
  # POST /admin/taxonomies.json
  def create
    @taxonomy = Taxonomy.new(taxonomy_params)

    respond_to do |format|
      if @taxonomy.save
        @seomore=Seomore.create
        @taxonomy.update(seomore_id: @seomore.id)
        instance_saver(@taxonomy, params)
        format.html { redirect_to admin_taxonomies_list_path(@taxonomy.record_type_id), :gflash => {:success => t('created taxonomy item')} }
        format.json { render action: 'show', status: :created, location: @taxonomy }
      else
        format.html { render action: 'new' }
        format.json { render json: @taxonomy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/taxonomies/1
  # PATCH/PUT /admin/taxonomies/1.json
  def update
    respond_to do |format|
      if @taxonomy.update(taxonomy_params)
        instance_saver(@taxonomy, params)
        format.html { redirect_to admin_taxonomies_list_path(@taxonomy.record_type_id), :gflash => {:success => t('updated taxonomy item')} }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @taxonomy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/taxonomies/1
  # DELETE /admin/taxonomies/1.json
  def destroy
    @taxonomy.destroy
    respond_to do |format|
      format.html { redirect_to admin_taxonomies_list_path(@taxonomy.record_type_id), :gflash => {:success => t('deleted taxonomy item')} }
      format.json { head :no_content }
    end
  end

  def sort
    params[:taxonomy].sort { |a, b| a <=> b }.each_with_index do |id, index|
      value = id[1][:id].to_i
      position = id[1][:position]
      position = position.to_i + 1
      parent = id[1][:parent_id] == 'null' ? '' : id[1][:parent_id]
      Taxonomy.find(value).update_attributes(:position => position, :parent_id => parent)
    end
    render :nothing => true
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_taxonomy
    @taxonomy = Taxonomy.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def taxonomy_params
    params.require(:taxonomy).permit(:public, :blocked, :url, :taxonomy_id, :position, :record_type_id, :parent_id)
  end
end
