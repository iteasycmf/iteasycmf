class Admin::UsersRolesController < ApplicationController
  before_action :set_users_role, only: [:show, :edit, :update, :destroy]

  # GET /admin/users_roles
  # GET /admin/users_roles.json
  def index
    @users_roles = UsersRole.all
  end

  # GET /admin/users_roles/1
  # GET /admin/users_roles/1.json
  def show
  end

  # GET /admin/users_roles/new
  def new
    @users_role = UsersRole.new
  end

  # GET /admin/users_roles/1/edit
  def edit
  end

  # POST /admin/users_roles
  # POST /admin/users_roles.json
  def create
    @users_role = UsersRole.new(users_role_params)

    respond_to do |format|
      if @users_role.save
        format.html { redirect_to :back, notice: 'Users role was successfully created.' }
        format.json { render action: 'show', status: :created, location: @users_role }
      else
        format.html { render action: 'new' }
        format.json { render json: @users_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/users_roles/1
  # PATCH/PUT /admin/users_roles/1.json
  def update
    respond_to do |format|
      if @users_role.update(users_role_params)
        format.html { redirect_to :back, notice: 'Users role was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @users_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/users_roles/1
  # DELETE /admin/users_roles/1.json
  def destroy
    @users_role.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Users role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_users_role
    @users_role = UsersRole.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def users_role_params
    params.require(:users_role).permit(:user_id, :role_id)
  end
end
