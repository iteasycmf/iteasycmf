class Admin::FieldCustomsController < Admin::BaseController
  before_action :set_field_custom, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  load_and_authorize_resource
  # GET /admin/field_customs
  # GET /admin/field_customs.json
  def index
    @field_customs = FieldCustom.all
  end

  # GET /admin/field_customs/1
  # GET /admin/field_customs/1.json
  def show
  end

  # GET /admin/field_customs/new
  def new
    @field_custom = FieldCustom.new
  end

  # GET /admin/field_customs/1/edit
  def edit
  end

  # POST /admin/field_customs
  # POST /admin/field_customs.json
  def create
    @field_custom = FieldCustom.new(field_custom_params)

    respond_to do |format|
      if @field_custom.save
        format.html { redirect_to [:admin, @field_custom], notice: 'Field custom was successfully created.' }
        format.json { render action: 'show', status: :created, location: @field_custom }
      else
        format.html { render action: 'new' }
        format.json { render json: @field_custom.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/field_customs/1
  # PATCH/PUT /admin/field_customs/1.json
  def update
    respond_to do |format|
      if @field_custom.update(field_custom_params)
        format.html { redirect_to [:admin, @field_custom], notice: 'Field custom was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @field_custom.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/field_customs/1
  # DELETE /admin/field_customs/1.json
  def destroy
    @field_custom.destroy
    respond_to do |format|
      format.html { redirect_to admin_field_customs_url, notice: 'Field custom was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_field_custom
    @field_custom = FieldCustom.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def field_custom_params
    params.require(:field_custom).permit(:name, :machine_name, :active, :value)
  end
end
