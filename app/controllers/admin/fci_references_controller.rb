class Admin::FciReferencesController < ApplicationController
  before_action :set_fci_reference, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  # GET /admin/fci_references
  # GET /admin/fci_references.json
  def index
    @fci_references = FciReference.all
  end

  # GET /admin/fci_references/1
  # GET /admin/fci_references/1.json
  def show
  end

  # GET /admin/fci_references/new
  def new
    @fci_reference = FciReference.new
  end

  # GET /admin/fci_references/1/edit
  def edit
  end

  # POST /admin/fci_references
  # POST /admin/fci_references.json
  def create
    @fci_reference = FciReference.new(fci_reference_params)

    respond_to do |format|
      if @fci_reference.save
        format.html { redirect_to [:admin, @fci_reference], notice: 'Fci reference was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fci_reference }
      else
        format.html { render action: 'new' }
        format.json { render json: @fci_reference.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/fci_references/1
  # PATCH/PUT /admin/fci_references/1.json
  def update
    respond_to do |format|
      if @fci_reference.update(fci_reference_params)
        format.html { redirect_to [:admin, @fci_reference], notice: 'Fci reference was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fci_reference.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/fci_references/1
  # DELETE /admin/fci_references/1.json
  def destroy
    @fci_reference.destroy
    respond_to do |format|
      format.html { redirect_to admin_fci_references_url, notice: 'Fci reference was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_reference
    @fci_reference = FciReference.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fci_reference_params
    params.require(:fci_reference).permit(:fci_id, :mid, :model, :rmodel, :rrecord_type, :rmid)
  end
end
