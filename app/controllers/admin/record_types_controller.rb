class Admin::RecordTypesController < Admin::BaseController
  before_action :set_record_type, only: [:show, :edit, :update, :destroy, :fields]
  layout 'admin'
  load_and_authorize_resource
  # GET /admin/record_types
  # GET /admin/record_types.json
  def index
    @record_types = RecordType.all
    breadcrumbs.add t('listing record types'), admin_record_types_url
  end

  # GET /admin/record_types/1
  # GET /admin/record_types/1.json
  def show
  end

  # GET /admin/record_types/1
  def fields
    @fields = FieldCustom.all
    @fcinew =Fci.new
    breadcrumbs.add t('listing record types'), admin_record_types_url
    breadcrumbs.add t('fields')+' '+@record_type.name
  end

  # GET /admin/record_types/new
  def new
    @record_type = RecordType.new
    breadcrumbs.add t('listing record types'), admin_record_types_url
    breadcrumbs.add t('new record types')
  end

  # GET /admin/record_types/1/edit
  def edit
    breadcrumbs.add t('listing record types'), admin_record_types_url
    breadcrumbs.add t('edit record types')
  end

  # POST /admin/record_types
  # POST /admin/record_types.json
  def create
    @record_type = RecordType.new(record_type_params)

    respond_to do |format|
      if @record_type.save
        format.html { redirect_to edit_admin_record_type_path(@record_type), :gflash => {:success => t('created recordtype')} }
        format.json { render action: 'show', status: :created, location: @record_type }
      else
        format.html { render action: 'new' }
        format.json { render json: @record_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/record_types/1
  # PATCH/PUT /admin/record_types/1.json
  def update
    respond_to do |format|
      if @record_type.update(record_type_params)
        format.html { redirect_to edit_admin_record_type_path(@record_type), :gflash => {:success => t('edited recordtype')} }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @record_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/record_types/1
  # DELETE /admin/record_types/1.json
  def destroy
    @record_type.destroy
    respond_to do |format|
      format.html { redirect_to admin_record_types_url, :gflash => {:success => t('destroy recordtype')} }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_record_type
    @record_type = RecordType.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def record_type_params
    params.require(:record_type).permit(:name, :machine_name, :desc, :help, :has_title, :label_title, :disabled,
                                        :model, :nesting, :position, :comment_type, :relation, :path_root_url)
  end
end
