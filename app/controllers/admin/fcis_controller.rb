class Admin::FcisController < Admin::BaseController
  skip_before_filter :verify_authenticity_token
  before_action :set_fci, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  load_and_authorize_resource
  # GET /admin/fcis
  # GET /admin/fcis.json
  def index
    @fcis = Fci.all
  end

  # GET /admin/fcis/1
  # GET /admin/fcis/1.json
  def show
  end

  # GET /admin/fcis/new
  def new
    @fci = Fci.new
  end

  # GET /admin/fcis/1/edit
  def edit
  end

  # POST /admin/fcis
  # POST /admin/fcis.json
  def create
    @fci = Fci.new(fci_params)

    respond_to do |format|
      if @fci.save
        @fcinew =Fci.new
        @fields = FieldCustom.all
        @record_type = @fci.record_type
        format.html { redirect_to [:admin, @fci], notice: 'Fci was successfully created.' }
        format.js
        format.json { render action: 'show', status: :created, location: @fci }
      else
        format.html { render action: 'new' }
        format.json { render json: @fci.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PATCH/PUT /admin/fcis/1
  # PATCH/PUT /admin/fcis/1.json
  def update
    respond_to do |format|
      if @fci.update(fci_params)
        @fcinew =Fci.new
        @fields = FieldCustom.all
        @record_type = @fci.record_type
        format.html { redirect_to [:admin, @fci], notice: 'Fci was successfully updated.' }
        format.json { head :no_content }
        format.js
      else
        format.html { render action: 'edit' }
        format.json { render json: @fci.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /admin/fcis/1
  # DELETE /admin/fcis/1.json
  def destroy
    fci = @fci
    @fci.destroy
    @fcis = Fci.where(model: @fci.model, mid: @fci.mid)
    respond_to do |format|
      @record_type = fci.record_type
      format.html { redirect_to :back, notice: 'Fci was successfully destroyed.' }
      format.js
      format.json { head :no_content }
    end
  end

  def sort
    params[:order].each do |key, value|
      unless value[:id].nil?
        Fci.find(value[:id]).update_attribute(:position, value[:position])
      end
    end
    @fcinew =Fci.new
    @fields = FieldCustom.all
    render :nothing => true
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_fci
    @fci = Fci.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fci_params
    params.require(:fci).permit(:name, :machine_name, :active, :field_custom_id, :mid, :model, :position,
                                :placeholder, :required_field, :value, :prefix, :suffix, :elementfield,
                                :classfield, :idfield, :filter, :multi, :help)
  end
end
