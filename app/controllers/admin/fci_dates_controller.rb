class Admin::FciDatesController < Admin::BaseController
  before_action :set_fci_date, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  # GET /admin/fci_dates
  # GET /admin/fci_dates.json
  def index
    @fci_dates = FciDate.all
  end

  # GET /admin/fci_dates/1
  # GET /admin/fci_dates/1.json
  def show
  end

  # GET /admin/fci_dates/new
  def new
    @fci_date = FciDate.new
  end

  # GET /admin/fci_dates/1/edit
  def edit
  end

  # POST /admin/fci_dates
  # POST /admin/fci_dates.json
  def create
    @fci_date = FciDate.new(fci_date_params)

    respond_to do |format|
      if @fci_date.save
        format.html { redirect_to [:admin, @fci_date], notice: 'Fci date was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fci_date }
      else
        format.html { render action: 'new' }
        format.json { render json: @fci_date.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/fci_dates/1
  # PATCH/PUT /admin/fci_dates/1.json
  def update
    respond_to do |format|
      if @fci_date.update(fci_date_params)
        format.html { redirect_to [:admin, @fci_date], notice: 'Fci date was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fci_date.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/fci_dates/1
  # DELETE /admin/fci_dates/1.json
  def destroy
    @fci_date.destroy
    respond_to do |format|
      format.html { redirect_to admin_fci_dates_url, notice: 'Fci date was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_date
    @fci_date = FciDate.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fci_date_params
    params.require(:fci_date).permit(:value, :fci, :mid, :model)
  end
end
