class Admin::FciDecimalsController < Admin::BaseController
  before_action :set_fci_decimal, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  # GET /admin/fci_decimals
  # GET /admin/fci_decimals.json
  def index
    @fci_decimals = FciDecimal.all
  end

  # GET /admin/fci_decimals/1
  # GET /admin/fci_decimals/1.json
  def show
  end

  # GET /admin/fci_decimals/new
  def new
    @fci_decimal = FciDecimal.new
  end

  # GET /admin/fci_decimals/1/edit
  def edit
  end

  # POST /admin/fci_decimals
  # POST /admin/fci_decimals.json
  def create
    @fci_decimal = FciDecimal.new(fci_decimal_params)

    respond_to do |format|
      if @fci_decimal.save
        format.html { redirect_to [:admin, @fci_decimal], notice: 'Fci decimal was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fci_decimal }
      else
        format.html { render action: 'new' }
        format.json { render json: @fci_decimal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/fci_decimals/1
  # PATCH/PUT /admin/fci_decimals/1.json
  def update
    respond_to do |format|
      if @fci_decimal.update(fci_decimal_params)
        format.html { redirect_to [:admin, @fci_decimal], notice: 'Fci decimal was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fci_decimal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/fci_decimals/1
  # DELETE /admin/fci_decimals/1.json
  def destroy
    @fci_decimal.destroy
    respond_to do |format|
      format.html { redirect_to admin_fci_decimals_url, notice: 'Fci decimal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_decimal
    @fci_decimal = FciDecimal.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fci_decimal_params
    params.require(:fci_decimal).permit(:value, :fci, :mid, :model)
  end
end
