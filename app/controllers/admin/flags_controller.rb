class Admin::FlagsController < ApplicationController
  before_action :set_flag, only: [:show, :edit, :update, :destroy]

  # GET /admin/flags
  # GET /admin/flags.json
  def index
    @flags = Flag.all
  end

  # GET /admin/flags/1
  # GET /admin/flags/1.json
  def show
  end

  # GET /admin/flags/new
  def new
    @flag = Flag.new
  end

  # GET /admin/flags/1/edit
  def edit
  end

  # POST /admin/flags
  # POST /admin/flags.json
  def create
    @flag = Flag.new(flag_params)

    respond_to do |format|
      if @flag.save
        format.html { redirect_to [:admin, @flag], notice: 'Flag was successfully created.' }
        format.json { render action: 'show', status: :created, location: @flag }
      else
        format.html { render action: 'new' }
        format.json { render json: @flag.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/flags/1
  # PATCH/PUT /admin/flags/1.json
  def update
    respond_to do |format|
      if @flag.update(flag_params)
        format.html { redirect_to [:admin, @flag], notice: 'Flag was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @flag.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/flags/1
  # DELETE /admin/flags/1.json
  def destroy
    @flag.destroy
    respond_to do |format|
      format.html { redirect_to admin_flags_url, notice: 'Flag was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_flag
      @flag = Flag.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def flag_params
      params.require(:flag).permit(:user_id, :mid, :model, :type_flag, :position)
    end
end
