class Admin::FciTextsController < Admin::BaseController
  before_action :set_fci_text, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  # GET /admin/fci_texts
  # GET /admin/fci_texts.json
  def index
    @fci_texts = FciText.all
  end

  # GET /admin/fci_texts/1
  # GET /admin/fci_texts/1.json
  def show
  end

  # GET /admin/fci_texts/new
  def new
    @fci_text = FciText.new
  end

  # GET /admin/fci_texts/1/edit
  def edit
  end

  # POST /admin/fci_texts
  # POST /admin/fci_texts.json
  def create
    @fci_text = FciText.new(fci_text_params)

    respond_to do |format|
      if @fci_text.save
        format.html { redirect_to [:admin, @fci_text], notice: 'Fci text was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fci_text }
      else
        format.html { render action: 'new' }
        format.json { render json: @fci_text.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/fci_texts/1
  # PATCH/PUT /admin/fci_texts/1.json
  def update
    respond_to do |format|
      if @fci_text.update(fci_text_params)
        format.html { redirect_to [:admin, @fci_text], notice: 'Fci text was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fci_text.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/fci_texts/1
  # DELETE /admin/fci_texts/1.json
  def destroy
    @fci_text.destroy
    respond_to do |format|
      format.html { redirect_to admin_fci_texts_url, notice: 'Fci text was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_text
    @fci_text = FciText.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fci_text_params
    params.require(:fci_text).permit(:value, :shortvalue, :fci, :mid, :model, :format_field)
  end
end
