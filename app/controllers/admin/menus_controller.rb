class Admin::MenusController < Admin::BaseController
  before_action :set_menu, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  load_and_authorize_resource
  #load_and_authorize_resource
  # GET /admin/menus
  # GET /admin/menus.json
  def index
    @menus = Menu.all
    breadcrumbs.add t('listing menus'), admin_menus_url
    @menu = Menu.new
  end

  # GET /admin/menus/1
  # GET /admin/menus/1.json
  def show
    @menu_item=MenuItem.new
    breadcrumbs.add t('listing menus'), admin_menus_url
    breadcrumbs.add t('listing menu_items')+' '+@menu.name
  end

  # GET /admin/menus/new
  def new
    @menu = Menu.new
  end

  # GET /admin/menus/1/edit
  def edit
  end

  # POST /admin/menus
  # POST /admin/menus.json
  def create
    @menu = Menu.new(menu_params)

    respond_to do |format|
      if @menu.save
        format.html { redirect_to admin_menu_url(@menu), :gflash => {:success => t('created menu')+" "+@menu.name} }
        format.json { render action: 'show', status: :created, location: @menu }
      else
        format.html { render action: 'new' }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/menus/1
  # PATCH/PUT /admin/menus/1.json
  def update
    breadcrumbs.add t('listing menus'), admin_menus_url
    breadcrumbs.add t('editing menu')
    respond_to do |format|
      if @menu.update(menu_params)
        format.html { redirect_to admin_menus_url, :gflash => {:success => t('updated menu')+" "+@menu.name} }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/menus/1
  # DELETE /admin/menus/1.json
  def destroy
    unless @menu.blocked?
      @menu.destroy
    end
    respond_to do |format|
      format.html { redirect_to admin_menus_url, notice: 'Menu was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_menu
    @menu = Menu.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def menu_params
    params.require(:menu).permit(:name, :desc, :public, :block_id, :machine_name, :csscls)
  end
end
