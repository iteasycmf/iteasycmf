class Admin::MenuItemsController < Admin::BaseController
  skip_before_filter :verify_authenticity_token
  before_action :set_menu_item, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  load_and_authorize_resource
  respond_to :js
  # GET /admin/menu_items
  # GET /admin/menu_items.json
  def index
    @menu_items = MenuItem.all
  end

  # GET /admin/menu_items/1
  # GET /admin/menu_items/1.json
  def show

  end

  # GET /admin/menu_items/new
  def new
    @menu_item = MenuItem.new
  end

  # GET /admin/menu_items/1/edit
  def edit
  end

  # POST /admin/menu_items
  # POST /admin/menu_items.json
  def create
    @menu_item = MenuItem.new(menu_item_params)

    respond_to do |format|
      if @menu_item.save
        @menu_itemnew=MenuItem.new
        @menu=@menu_item.menu
        format.html { redirect_to :back, :gflash => {:success => t('created menu item')+" "+@menu_item.name} }
        format.json { render action: 'show', status: :created, location: @menu_item }
      else
        format.html { render action: 'new' }
        format.json { render json: @menu_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/menu_items/1
  # PATCH/PUT /admin/menu_items/1.json
  def update
    respond_to do |format|
      if @menu_item.update(menu_item_params)
        @menu_itemnew=MenuItem.new
        @menu=@menu_item.menu

        format.html { redirect_to admin_menu_path(@menu_item.menu), :gflash => {:success => t('updated menu item')+" "+@menu_item.name} }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @menu_item.errors, status: :unprocessable_entity }
      end
    end
    breadcrumbs.add t('listing menus'), admin_menus_url
    breadcrumbs.add t('editing menu_item')
  end

  # DELETE /admin/menu_items/1
  # DELETE /admin/menu_items/1.json
  def destroy
    @mi = @menu_item
    @menu_item.destroy
    respond_to do |format|
      @menu_itemnew=MenuItem.new
      @menu=@mi.menu
      format.html { redirect_to admin_menu_path(@menu_item.menu), notice: 'Menu item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def sort
    params[:menu_item].sort { |a, b| a <=> b }.each_with_index do |id, index|
      value = id[1][:id].to_i
      position = id[1][:position]
      position = position.to_i + 1
      parent = id[1][:parent_id] == 'null' ? '' : id[1][:parent_id]
      MenuItem.find(value).update_attributes(:position => position, :parent_id => parent)
    end
    render :nothing => true
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_menu_item
    @menu_item = MenuItem.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def menu_item_params
    params.require(:menu_item).permit(:name, :url, :title, :menu_id, :menu_item_id, :public, :position, :csscls,
                                      :parent_id)
  end

  protected

end
