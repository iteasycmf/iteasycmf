class Admin::UsersController < Admin::BaseController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  load_and_authorize_resource
  # GET /admin/users
  # GET /admin/users.json
  def index
    breadcrumbs.add t('listing users'), admin_users_url
    @users = User.all
    respond_to do |format|
      format.html
      format.xls
    end
  end

  # GET /admin/users/1
  # GET /admin/users/1.json
  def show
  end

  # GET /admin/users/new
  def new
    @user = User.new
    breadcrumbs.add t('listing users'), admin_users_url
    breadcrumbs.add t('new user')
  end

  # GET /admin/users/1/edit
  def edit
    breadcrumbs.add t('listing users'), admin_users_url
    breadcrumbs.add t('edit')
  end

  # POST /admin/users
  # POST /admin/users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        profile = Profile.create!(record_type_id: 2, entity: :personals)
        @user.update!(profile_id: profile.id)
        instance_saver(@user.profile, params)
        unless params[:role].nil?
          UsersRole.create!(role_id: params[:role], user_id: @user.id)
        end
        UserMailer.welcome_email(@user).deliver_now
        format.html { redirect_to admin_users_url, notice: 'User was successfully created.' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/users/1
  # PATCH/PUT /admin/users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        instance_saver(@user.profile, params)
        format.html { redirect_to :back, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/users/1
  # DELETE /admin/users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:username, :email, :admin, :edit, :password, :password_confirmation,
                                 profile_attributes: [:record_type_id])
  end
end
