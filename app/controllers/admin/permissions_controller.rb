class Admin::PermissionsController < Admin::BaseController
  before_action :set_permission, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  load_and_authorize_resource
  # GET /admin/permissions
  # GET /admin/permissions.json
  def index
    @permissions = Permission.all
  end

  # GET /admin/permissions/1
  # GET /admin/permissions/1.json
  def show
  end

  # GET /admin/permissions/new
  def new
    @permission = Permission.new
  end

  # GET /admin/permissions/1/edit
  def edit
  end

  # POST /admin/permissions
  # POST /admin/permissions.json
  def create
    @permission = Permission.new(permission_params)

    respond_to do |format|
      if @permission.save
        format.html { redirect_to [:admin, @permission], notice: 'Permission was successfully created.' }
        format.json { render action: 'show', status: :created, location: @permission }
      else
        format.html { render action: 'new' }
        format.json { render json: @permission.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/permissions/1
  # PATCH/PUT /admin/permissions/1.json
  def update
    respond_to do |format|
      if @permission.update(permission_params)
        format.html { redirect_to [:admin, @permission], notice: 'Permission was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @permission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/permissions/1
  # DELETE /admin/permissions/1.json
  def destroy
    @permission.destroy
    respond_to do |format|
      format.html { redirect_to admin_permissions_url, notice: 'Permission was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_permission
    @permission = Permission.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def permission_params
    params.require(:permission).permit(:role_id, :name, :subject_class, :subject_id, :action, :description, :active, :admin)
  end
end
