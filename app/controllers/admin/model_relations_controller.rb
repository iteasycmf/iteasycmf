class Admin::ModelRelationsController < ApplicationController
  before_action :set_model_relation, only: [:show, :edit, :update, :destroy]

  # GET /admin/model_relations
  # GET /admin/model_relations.json
  def index
    @model_relations = ModelRelation.all
  end

  # GET /admin/model_relations/1
  # GET /admin/model_relations/1.json
  def show
  end

  # GET /admin/model_relations/new
  def new
    @model_relation = ModelRelation.new
  end

  # GET /admin/model_relations/1/edit
  def edit
  end

  # POST /admin/model_relations
  # POST /admin/model_relations.json
  def create
    @model_relation = ModelRelation.new(model_relation_params)
    respond_to do |format|
      if @model_relation.save
        if @model_relation.model_main == 'Record'
          @record = Record.find(@model_relation.mid_main)
        elsif @model_relation.model_main == 'Block'
          @block = Block.find(@model_relation.mid_main)
        end
        format.html { redirect_to [:admin, @model_relation], notice: 'Model relation was successfully created.' }
        format.js
        format.json { render action: 'show', status: :created, location: @model_relation }
      else
        format.html { render action: 'new' }
        format.json { render json: @model_relation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/model_relations/1
  # PATCH/PUT /admin/model_relations/1.json
  def update
    respond_to do |format|
      if @model_relation.update(model_relation_params)
        format.html { redirect_to [:admin, @model_relation], notice: 'Model relation was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @model_relation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/model_relations/1
  # DELETE /admin/model_relations/1.json
  def destroy
    @model_relation.destroy
    respond_to do |format|
      @record = Record.find(@model_relation.mid_main) if @model_relation.model_main == 'Record'
      format.html { redirect_to :back, notice: 'Model relation was successfully destroyed.' }
      format.json { head :no_content }
      format.js {}
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_model_relation
    @model_relation = ModelRelation.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def model_relation_params
    params.require(:model_relation).permit(:mid_main, :model_main, :mid, :model, :record_type)
  end
end
