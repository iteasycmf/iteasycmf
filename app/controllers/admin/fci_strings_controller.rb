class Admin::FciStringsController < Admin::BaseController
  before_action :set_fci_string, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  load_and_authorize_resource
  # GET /admin/fci_strings
  # GET /admin/fci_strings.json
  def index
    @fci_strings = FciString.all
  end

  # GET /admin/fci_strings/1
  # GET /admin/fci_strings/1.json
  def show
  end

  # GET /admin/fci_strings/new
  def new
    @fci_string = FciString.new
  end

  # GET /admin/fci_strings/1/edit
  def edit
  end

  # POST /admin/fci_strings
  # POST /admin/fci_strings.json
  def create
    @fci_string = FciString.new(fci_string_params)

    respond_to do |format|
      if @fci_string.save
        format.html { redirect_to [:admin, @fci_string], notice: 'Fci string was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fci_string }
      else
        format.html { render action: 'new' }
        format.json { render json: @fci_string.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/fci_strings/1
  # PATCH/PUT /admin/fci_strings/1.json
  def update
    respond_to do |format|
      if @fci_string.update(fci_string_params)
        format.html { redirect_to [:admin, @fci_string], notice: 'Fci string was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fci_string.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/fci_strings/1
  # DELETE /admin/fci_strings/1.json
  def destroy
    @fci_string.destroy
    respond_to do |format|
      format.html { redirect_to admin_fci_strings_url, notice: 'Fci string was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_string
    @fci_string = FciString.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fci_string_params
    params.require(:fci_string).permit(:value, :fci, :mid, :model)
  end
end
