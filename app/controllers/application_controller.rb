class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include ApplicationHelper
  protect_from_forgery with: :exception, prepend: true
  theme :theme_resolver
  before_action :set_locale
  after_action :profile
  #before_filter :add_initial_breadcrumbs
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_global_search_variable
  before_action :set_current_user
  rescue_from ActionController::RoutingError, :with => :render_404
  use_growlyflash

  before_filter :reject_locked!, if: :devise_controller?

        # Auto-sign out locked users
        def reject_locked!
        if current_user && current_user.access_locked?

            respond_to do |format|
                format.html { redirect_to root_url+'spasibo_za_registratsiyu' }
                format.json { render :status => 401, :json => {:error => "Your account is locked."}}
            end

        end
        end



  def set_current_user
    User.current = current_user
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :notice => exception.message
  end

  def set_global_search_variable
    @ls = FciString.where(model: 'Record').ransack(params[:q])
  end

  def set_locale
    if params[:locale].present?
      I18n.locale = params[:locale]
    else
      #if http_accept_language.compatible_language_from(I18n.available_locales) == System.find_by_key(:locale).value.to_sym
      #I18n.locale = System.find_by_key(:locale).value.to_sym
      #else
      I18n.locale = System.find_by_key(:locale).value.to_sym
      #end
    end
    cookies.permanent[:educator_locale] = I18n.locale
  end


  def default_url_options(options={})
    {locale: I18n.locale}
  end


  def theme_setting
    YAML.load_file('app/themes/'+theme_resolver+'/setting.yml')
  end


  def localecmf
    System.find_by_key(:locale).value.to_sym
  end

  def localeroute
    System.find_by_key(:localeroute).value.to_sym
  end

  def profile
    if user_signed_in?
      if current_user.profile_id.nil? || current_user.profile_id == 0
        @rt= RecordType.find_by_machine_name(:personals)
        @profile = Profile.new
        @profile.record_type_id= @rt.id.to_i
        @profile.save
        @user=User.find(current_user.id)
        @user.update(profile_id: @profile.id)
        @user.save

      end
    end
  end

  def locale_empty_translation
    System.find_by_ key(:locale_empty).value.to_sym
  end

  def add_initial_breadcrumbs
    breadcrumbs.add t('home'), root_path
  end



  private


  protected

  def configure_permitted_parameters
    added_attrs = [:username, :email, :password, :password_confirmation, :remember_me]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

  # Derive the model name from the controller. egs UsersController will return User
  def self.permission
    return name = controller_name.classify.constantize
  end

  #Очень плохое временное решение по мультиполям
  def instance_saver(instance, params)
    instance.fcis.each do |fci|
      parsed =JSON.parse(fci.field_custom.value)
      parsed['param'].each do |item|
        line = item['value']+fci.machine_name
        if params[':'+line].present?
          model = parsed['model'].constantize
          if fci.multi?
            #n=0
            params[':'+line].each do |line_item|
            #  field = model.where(fci_id: fci.id, model: instance.model_name.human, mid: instance.id).offset(n).take
             # n+=1
             # if field.nil?
                field_new = model.create(mid: instance.id, model: instance.model_name.name, fci_id: fci.id)
                field_new.update(item['line'].to_sym => line_item)
              #else
              #  field.update(item['line'].to_sym => line_item)
             # end
            end
          else
            field = model.find_by(fci_id: fci.id, model: instance.model_name.human, mid: instance.id)
            if field.nil?
              field_new = model.create(mid: instance.id, model: instance.model_name.name, fci_id: fci.id)
              field_new.update(item['line'].to_sym => params[':'+line])
            else
              field.update(item['line'].to_sym => params[':'+line])
            end
          end
        end
      end
    end
  end


  def after_sign_in_path_for(resource)
    sign_in_url = new_user_session_url
    if request.referer == sign_in_url
      super
    else
      stored_location_for(resource) || request.referer || root_path
    end
  end

end
