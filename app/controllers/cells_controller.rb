class CellsController < ApplicationController
  before_action :set_cell, only: [:show, :edit, :update, :destroy]
  respond_to :js
  # GET /admin/cells
  # GET /admin/cells.json
  def index
    @cells = Cell.all
  end

  # GET /admin/cells/1
  # GET /admin/cells/1.json
  def show
  end

  # GET /admin/cells/new
  def new
    @cell = Cell.new
  end

  # GET /admin/cells/1/edit
  def edit
  end

  # POST /admin/cells
  # POST /admin/cells.json
  def create
    @cell = Cell.new(cell_params)
    respond_to do |format|
      if @cell.save
        instance_saver(@cell, params)
        mr = ModelRelation.create(mid_main: params[:mid_main], model_main: params[:model_main], mid: @cell.id, model: 'Cell', record_type: @cell.record_type.machine_name)
        if mr.model_main == 'Record'
          @cells = Record.find(mr.mid_main).cells
        elsif mr.model_main == 'Profile'
          @profile = Profile.find(mr.mid_main)
          @cells = @profile.cells
        elsif mr.model_main == 'Block'
          @cells = Block.find(mr.mid_main).cells
        end
        format.html { redirect_to :back, notice: 'Запись была добавлена.' }
        format.json { render action: 'show', status: :created, location: @cell }
        format.js {}
      else
        format.html { render action: 'new' }
        format.json { render json: @cell.errors, status: :unprocessable_entity }
        format.js {}
      end
    end
  end

  # PATCH/PUT /admin/cells/1
  # PATCH/PUT /admin/cells/1.json
  def update
    respond_to do |format|
      if @cell.update(cell_params)
        instance_saver(@cell, params)
        format.html { redirect_to :back, notice: 'Запись была обновлена.' }
        format.json { head :no_content }
        format.js {}
      else
        format.html { render action: 'edit' }
        format.json { render json: @cell.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/cells/1
  # DELETE /admin/cells/1.json
  def destroy
    @mm = @cell.reverse_model_relations.first.mid_main
    @model_main = @cell.reverse_model_relations.first.model_main
    @cell.destroy
    respond_to do |format|
      if @model_main == 'Record'
        @cells = Record.find(@mm).cells
      else
        @cells = Block.find(@mm).cells
      end
      format.html { redirect_to :back, notice: 'Запись была удалена.' }
      format.json { head :no_content }
      format.js {}
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_cell
    @cell = Cell.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def cell_params
    params.require(:cell).permit(:record_type_id, :public, :position, :group)
  end
end
