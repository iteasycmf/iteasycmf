class RecordsController < ApplicationController
  before_action :set_record, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  include ApplicationHelper

  # GET /records
  # GET /records.json
  def index
    @records = Record.all

  end

  # GET /records/1
  # GET /records/1.json
  def show
    @record = Record.friendly.find(params[:id])
    @model_relation= ModelRelation.new
    @comment=Comment.new
    @comments = Comment.where(model: 'Record', mid: @record.id)
    @q = Record.where(public: true).search(params[:q])
    @lists = @q.result.page params[:page]
    @tax = Record.where(public: true, record_type_id: 8).search(params[:q])
    @lists2 = @tax.result.uniq.page params[:page]
    @cell = Cell.where(record_type_id: 9, public: true).search(params[:q])
    @celllists = @cell.result.uniq.page params[:page]
    breadcrumbs.add t('home'), root_url
    taxs = @record.taxonomies
    unless taxs.nil?
      taxs.each do |tax|
        breadcrumbs.add tax.title, model_url(tax)
      end
    end
    breadcrumbs.add @record.title
  end

  # GET /records/new
  def new
    @record = Record.new
  end

  # GET /records/1/edit
  def edit
  end

  # POST /records
  # POST /records.json
  def create
    @record = Record.new(record_params)

    respond_to do |format|
      if @record.save
        format.html { redirect_to @record, notice: 'Record was successfully created.' }
        format.json { render :show, status: :created, location: @record }
      else
        format.html { render :new }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /records/1
  # PATCH/PUT /records/1.json
  def update
    respond_to do |format|
      if @record.update(record_params)
        format.html { redirect_to @record, notice: 'Record was successfully updated.' }
        format.json { render :show, status: :ok, location: @record }
      else
        format.html { render :edit }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /records/1
  # DELETE /records/1.json
  def destroy
    @record.destroy
    respond_to do |format|
      format.html { redirect_to records_url, notice: 'Record was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.htkm
  def set_record
    @record = Record.friendly.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def record_params
    params[:record]
  end
end
