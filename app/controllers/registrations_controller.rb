class RegistrationsController < Devise::RegistrationsController
  respond_to :json

  def new
    super

  end

  def create
    super
    @profile = Profile.new
    @profile.record_type_id= 2
    @profile.save
    @user.update(profile_id: @profile.id)
    @user.save
    instance_saver(@profile, params)
    ChatRoom.create(title: 'chat', user_id: @user.id, public:false)
  end

  def update
    super
    instance_saver(current_user.profile, params)
  end

  def after_update_path_for(resource)
    case resource
      when :user, User
        root_url+'users/edit'
      else
        super
    end
  end
end
