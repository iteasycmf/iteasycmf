class TaxonomiesController < ApplicationController
  before_action :set_taxonomy, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  respond_to :html, :json, :js
  include ApplicationHelper
  # GET /taxonomies
  # GET /taxonomies.json
  def index
    @taxonomies = Taxonomy.all
    @records = Record.all
    @search = ModelRelation.where(record_type: :product_category).search(params[:q])
    @products = @search.result
  end

  # GET /taxonomies/1
  # GET /taxonomies/1.json
  def show
    @tax = @taxonomy.records.where(public: true).search(params[:q])
    @lists = @tax.result.uniq.page(params[:page])
    breadcrumbs.add t('home'), root_url
    parent = @taxonomy.ancestors
    unless parent.nil?
      parent.each do |tax|
        breadcrumbs.add tax.title, model_url(tax)
      end
    end
    breadcrumbs.add @taxonomy.title
  end

  # GET /taxonomies/new
  def new
    @taxonomy = Taxonomy.new

  end

  # GET /taxonomies/1/edit
  def edit
  end

  # POST /taxonomies
  # POST /taxonomies.json
  def create
    @taxonomy = Taxonomy.new(taxonomy_params)

    respond_to do |format|
      if @taxonomy.save
        format.html { redirect_to @taxonomy, notice: 'Taxonomy was successfully created.' }
        format.json { render :show, status: :created, location: @taxonomy }
      else
        format.html { render :new }
        format.json { render json: @taxonomy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /taxonomies/1
  # PATCH/PUT /taxonomies/1.json
  def update
    respond_to do |format|
      if @taxonomy.update(taxonomy_params)
        format.html { redirect_to @taxonomy, notice: 'Taxonomy was successfully updated.' }
        format.json { render :show, status: :ok, location: @taxonomy }
      else
        format.html { render :edit }
        format.json { render json: @taxonomy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /taxonomies/1
  # DELETE /taxonomies/1.json
  def destroy
    @taxonomy.destroy
    respond_to do |format|
      format.html { redirect_to taxonomies_url, notice: 'Taxonomy was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_taxonomy
    @taxonomy = Taxonomy.friendly.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def taxonomy_params
    params.require(:taxonomy).permit(:public, :blocked, :url, :taxonomy_id, :position, :record_type_id)
  end
end
