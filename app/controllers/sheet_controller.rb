class SheetController < ApplicationController
  def search
    @ls = FciString.where(model: 'Record', fci_id: 14).ransack(params[:log_search], search_key: :log_search)
    @fci_strings = @ls.result.page params[:page]
    @model= Record.first
  end

  def robots
    @records = Record.where(public: false)
    @taxonomies = Taxonomy.where(public: false)
  end

  def feeds

    @records=Record.order(created_at: :desc)
  end

  def sitemap

  end

end
