class Record < ApplicationRecord
  belongs_to :record_type
  belongs_to :seomore, touch: true
  belongs_to :menu_item, touch: true
  has_many :fcis, through: :record_type
  RecordType.where(model: 'Record').includes(:fcis).each do |rt|
    rt.fcis.includes(:field_custom).each do |fci|
      has_many fci.machine_name.pluralize.to_sym, -> {where(model: 'Record', fci_id: fci.id)}, :foreign_key => 'mid', class_name: JSON.parse(fci.field_custom.value)['model']
    end
  end
  has_many :model_relations, -> {where(model_main: 'Record')}, :foreign_key => 'mid_main', dependent: :destroy
  has_many :taxonomies, through: :model_relations
  has_many :cells, through: :model_relations
  has_many :flags, -> {where(model: 'Record')}, :foreign_key => 'mid', dependent: :destroy
  accepts_nested_attributes_for :seomore, allow_destroy: true

  translates :url, :fallbacks_for_empty_translations => true
  extend FriendlyId
  friendly_id :url, :use => [:finders]

  paginates_per 14
  default_scope {order("created_at DESC")}

  before_save :set_entity
  after_save :urlsave


  def set_entity
    unless self.record_type_id.nil?
      if self.entity.nil?
        self.entity = self.record_type.machine_name
      end
    end
  end

  def taxonoms
    self.model_relations.where(mid_main: self.id).join(:taxonomy)
  end

  def urlsave
    self.url=self.id unless self.record_type.has_title?
  end

  def title
    type = self.record_type
    if type.has_title?
      fci = type.fcis.find_by(mid: type.id, name: type.label_title)
      label=FciString.includes(:translations).find_by(mid: self.id, model: 'Record', fci_id: fci.id)
      label.value unless label.nil?
    end
  end

  def cache_key
    super + '-' + Globalize.locale.to_s
  end


  def wizard_price
    client = Savon::Client.new(wsdl: 'http://149.202.222.162:80/base3s_admin_c/ws/WebSite.1cws?wsdl', basic_auth: ["callcenter", "Rfrf<zrf"])
    response = client.call(:price, message: {'Area'=> 'Украина'})
    comone = response.body[:price_response][:return]
    parsed = JSON.parse(comone)["#value"]
    parsed.each do |prod|
      uid = prod['UID']
      tov = FciString.find_by(fci_id: 85, model: 'Cell', value: uid)
      unless tov.nil?
        currency = prod['Currency']
        if currency == "Доллар"
          @c= 'USD'
        elsif currency == "Евро"
          @с = 'EUR'
        elsif currency == "Гривня"
          @c= 'UAH'
        elsif currency == "CZK"
          @c = 'CZK'
        elsif currency == "RUR"
          @c= 'RUB'
        end
        c=@c
        price = prod['Price']
        fciprice = FciPrice.find_by(fci_id: 20, mid: tov.mid, model: 'Cell')
        unless fciprice.nil?
          fciprice.update(value: price, currency: c)
        else
          FciPrice.find_by(fci_id: 20, mid: tov.mid, model: 'Cell',value: price, currency: c)
        end
      end
    end
  end

  def wizard
    client = Savon::Client.new(wsdl: 'http://149.202.222.162:80/base3s_admin_c/ws/WebSite.1cws?wsdl', basic_auth: ["callcenter", "Rfrf<zrf"])
    response = client.call(:goods)
    comone = response.body[:goods_response][:return]
    parsed = JSON.parse(comone)["#value"]
    #p = FciFile.new.parsed333
    parsed.each do |prod|
      uid = prod['UID']
      tov = FciString.find_by(fci_id: 85, value: uid)
      if tov.nil?
        code = prod['Code']
        article = prod['Article']
        clasif = prod['Classifikator']
        name = prod['Name']
        weight = prod['WeightBrutto']
        wnetto = prod['WeightNetto']
        messure = prod['UnityMeasure']
        strana = prod['manufacture']
        brand = prod['Brand']
        classifikator1 = prod['Classifikator1']
        classifikator2 = prod['Classifikator2']
        classifikator3 = prod['Classifikator3']
        palletnorma = prod['PalletNorma']
        nomengroup = prod['NomenklatureGroup']
        moq = prod['multipleOrderQuantity']
        cds = prod['CodeSectionOnSite']
        abs = prod['ClassifikatorABC']
        description = prod['Description']
        ros = prod['RegionOfSale']
        state = prod['State']
        length = prod['length']
        width = prod['width']
        size = prod['Size']
        pacamount = prod['PackageAmount']
        boxamount = prod['BoxAmount']
        supplier = prod['Supplier']
        currency = prod['Currency']


        s = Seomore.create
        record = Record.create(record_type_id: 8, public: true, seomore_id: s.id)
        cell = Cell.create(record_type_id: 9, public: true)
        ModelRelation.create(mid_main: record.id, model_main: 'Record', mid: cell.id, model: 'Cell', record_type: :stroka_tovara)
        FciString.create(fci_id: 14, mid: record.id, model: 'Record', value: name)
        FciText.create(fci_id: 16, mid: record.id, model: 'Record', value: description)
        FciString.create(fci_id: 85, mid: cell.id, model: 'Cell', value: uid)
        FciInteger.create(fci_id: 18, mid: cell.id, model: 'Cell', value: code)
        FciSku.create(fci_id: 19, mid: cell.id, model: 'Cell', value: article)
        FciString.create(fci_id: 17, mid: cell.id, model: 'Cell', value: name)
        FciString.create(fci_id: 86, mid: cell.id, model: 'Cell', value: ros)
        FciString.create(fci_id: 112, mid: cell.id, model: 'Cell', value: state)

        if currency == "Доллар"
          c= 'USD'
        elsif currency == "Евро"
          с = 'EUR'
        elsif currency == "Гривня"
          c= 'UAH'
        elsif currency == "CZK"
          c = 'CZK'
        elsif currency == "RUR"
          c= 'RUB'
        end
        FciPrice.create(fci_id: 20, mid: cell.id, model: 'Cell', value: 0, currency: c)
        unless abs == ""
          on = OptionName.find_by(value: 'Класификатор АВС')
          if on.nil?
            on = OptionName.create(name: 'Класификатор АВС', active: true)
          end
          OptionRelative.create(option_name_id: on.id, active: true, mid: record.id, model: 'Record')
          OptionValue.create(option_name_id: on.id, mid: record.id, model: 'Record', value: abs)
        end
        unless weight == ""
          on = OptionName.find_by(value: 'Вес с упаковкой')
          if on.nil?
            on = OptionName.create(name: 'Вес с упаковкой', active: true)
          end
          OptionRelative.create(option_name_id: on.id, active: true, mid: record.id, model: 'Record')
          OptionValue.create(option_name_id: on.id, mid: record.id, model: 'Record', value: weight)
        end
        unless wnetto == ""
          on = OptionName.find_by(value: 'Вес без упаковки')
          if on.nil?
            on = OptionName.create(name: 'Вес без упаковки', active: true)
          end
          OptionRelative.create(option_name_id: on.id, active: true, mid: record.id, model: 'Record')
          OptionValue.create(option_name_id: on.id, mid: record.id, model: 'Record', value: wnetto)
        end
        unless messure == ""
          on = OptionName.find_by(value: 'Мера')
          if on.nil?
            on = OptionName.create(name: 'Мера', active: true)
          end
          OptionRelative.create(option_name_id: on.id, active: true, mid: record.id, model: 'Record')
          OptionValue.create(option_name_id: on.id, mid: record.id, model: 'Record', value: messure)
        end
        unless strana == ""
          on = OptionName.find_by(value: 'Страна')
          if on.nil?
            on = OptionName.create(name: 'Страна', active: true)
          end
          OptionRelative.create(option_name_id: on.id, active: true, mid: record.id, model: 'Record')
          OptionValue.create(option_name_id: on.id, mid: record.id, model: 'Record', value: strana)
        end
        unless length == ""
          on = OptionName.find_by(value: 'Длина')
          if on.nil?
            on = OptionName.create(name: 'Длина', active: true)
          end
          OptionRelative.create(option_name_id: on.id, active: true, mid: record.id, model: 'Record')
          OptionValue.create(option_name_id: on.id, mid: record.id, model: 'Record', value: length)
        end
        unless width == ""
          on = OptionName.find_by(value: 'Ширина')
          if on.nil?
            on = OptionName.create(name: 'Ширина', active: true)
          end
          OptionRelative.create(option_name_id: on.id, active: true, mid: record.id, model: 'Record')
          OptionValue.create(option_name_id: on.id, mid: record.id, model: 'Record', value: width)
        end
        unless size == ""
          on = OptionName.find_by(value: 'Размер')
          if on.nil?
            on = OptionName.create(name: 'Размер', active: true)
          end
          OptionRelative.create(option_name_id: on.id, active: true, mid: record.id, model: 'Record')
          OptionValue.create(option_name_id: on.id, mid: record.id, model: 'Record', value: size)
        end

        unless brand == ""
          fsbrand = FciString.find_by(fci_id: 5, value: brand)
          if fsbrand
            ModelRelation.create(mid_main: record.id, model_main: 'Record', mid: fsbrand.mid, model: 'Taxonomy', record_type: :brendy)
          else
            s = Seomore.create
            tax = Taxonomy.create(public: true, record_type_id: 4, seomore_id: s.id)
            fsbrand = FciString.create(fci_id: 5, value: brand, mid: tax.id, model: 'Taxonomy')
            ModelRelation.create(mid_main: record.id, model_main: 'Record', mid: tax.id, model: 'Taxonomy', record_type: :brendy)
          end
        end

        unless classifikator1 == ""
          @mclassifikator1 = FciString.find_by(fci_id: 13, value: classifikator1)
          if @mclassifikator1
            ModelRelation.create(mid_main: record.id, model_main: 'Record', mid: @mclassifikator1.mid, model: 'Taxonomy', record_type: :category)
            @tax = Taxonomy.find(@mclassifikator1.mid)
          else
            s = Seomore.create
            @tax = Taxonomy.create(public: true, record_type_id: 7, seomore_id: s.id)
            @mclassifikator1 = FciString.create(fci_id: 13, value: classifikator1, mid: @tax.id, model: 'Taxonomy')
            ModelRelation.create(mid_main: record.id, model_main: 'Record', mid: @tax.id, model: 'Taxonomy', record_type: :category)
          end
          unless classifikator2 == ""
          @mclassifikator2 = @tax.children.search(zagolovoks_translations_value_eq: classifikator2).result.take
          if @mclassifikator2
            ModelRelation.create(mid_main: record.id, model_main: 'Record', mid: @mclassifikator2.id, model: 'Taxonomy', record_type: :category)
          else
            s = Seomore.create
            @mclassifikator2 = Taxonomy.create(public: true, record_type_id: 7, seomore_id: s.id, ancestry: "#{@tax.id}")
            FciString.create(fci_id: 13, value: classifikator2, mid: @mclassifikator2.id, model: 'Taxonomy')
            ModelRelation.create(mid_main: record.id, model_main: 'Record', mid: @mclassifikator2.id, model: 'Taxonomy', record_type: :category)
          end
          unless classifikator3 == ""
          @mclassifikator3 = Taxonomy.find(@mclassifikator2.id).children.search(zagolovoks_translations_value_eq: classifikator3).result.take
          if @mclassifikator3
            ModelRelation.create(mid_main: record.id, model_main: 'Record', mid: @mclassifikator3.id, model: 'Taxonomy', record_type: :category)
          else
            s = Seomore.create
            tax3 = Taxonomy.create(public: true, record_type_id: 7, ancestry: "#{@tax.id}/#{@mclassifikator2.id}", seomore_id: s.id)
            mclassifikator3 = FciString.create(fci_id: 13, value: classifikator3, mid: tax3.id, model: 'Taxonomy')
            ModelRelation.create(mid_main: record.id, model_main: 'Record', mid: tax3.id, model: 'Taxonomy', record_type: :category)
          end
        end
        end
        end
      end
    end
  end

  def teee
ModelRelation.where(record_type: :category).map{|e| e.destroy}
ModelRelation.where(record_type: :brendy).map{|e| e.destroy}
Cell.where(record_type_id: 9).map{|e| e.destroy}
Taxonomy.where(record_type_id: 7).map{|e| e.destroy}
Record.where(record_type_id: 8).map{|e| e.destroy}
FciString.where(fci_id: 13).map{|e| e.destroy}
FciString.where(fci_id: 14).map{|e| e.destroy}
FciString.where(fci_id: 85).map{|e| e.destroy}
FciString.where(fci_id:17).map{|e| e.destroy}
FciString.where(fci_id:86).map{|e| e.destroy}
FciString.where(fci_id:112).map{|e| e.destroy}
FciText.where(fci_id:16).map{|e| e.destroy}
FciInteger.where(fci_id:18).map{|e| e.destroy}
FciSku.where(fci_id:19).map{|e| e.destroy}
FciPrice.where(fci_id:20).map{|e| e.destroy}
FciString.where(fci_id: 5).map{|e| e.destroy}
Taxonomy.where(record_type_id: 4).map{|e| e.destroy}
Order.all.map{|e| e.destroy}
LineItem.all.map{|e| e.destroy}
OptionRelative.all.map{|e| e.destroy}
OptionValue.all.map{|e| e.destroy}
OptionName.all.map{|e| e.destroy}
  end

end
