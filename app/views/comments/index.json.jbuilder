json.array!(@comments) do |comment|
  json.extract! comment, :id, :user_id, :record_type_id, :mid, :model, :ancestry
  json.url comment_url(comment, format: :json)
end
