xml.instruct!
xml.rss version: '2.0', 'xmlns:atom' => 'http://www.w3.org/2005/Atom' do

  xml.channel do
    xml.title 'Название блога'
    xml.description 'Описание блога'
    xml.link root_url
    xml.language 'ru'
    xml.tag! 'atom:link', rel: 'self', type: 'application/rss+xml', href: 'home/rss'

    for record in @records
      xml.item do
        xml.title title(record)
        xml.link record_url(record)
        xml.pubDate(record.created_at.rfc2822)
        xml.guid record_url(record)
      end
    end

  end

end
