json.array!(@fci_integers) do |fci_integer|
  json.extract! fci_integer, :value, :fci_id, :mid, :model
  json.url fci_integer_url(fci_integer, format: :json)
end