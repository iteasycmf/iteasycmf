json.array!(@fci_texts) do |fci_text|
  json.extract! fci_text, :value, :shortvalue
  json.url fci_text_url(fci_text, format: :json)
end