json.array!(@model_relations) do |model_relation|
  json.extract! model_relation, :mid_main, :model_main, :mid, :model
  json.url model_relation_url(model_relation, format: :json)
end