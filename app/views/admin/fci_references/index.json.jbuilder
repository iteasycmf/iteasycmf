json.array!(@fci_references) do |fci_reference|
  json.extract! fci_reference, :fci_id, :mid, :model, :rmodel, :rrecord_type, :rmid
  json.url fci_reference_url(fci_reference, format: :json)
end