json.array!(@records) do |record|
  json.extract! record, :type, :record_type_id, :url, :status, :sticky, :seomore_id, :menu_item_id
  json.url record_url(record, format: :json)
end