json.array!(@record_types) do |record_type|
  json.extract! record_type, :name, :machine_name, :desc, :help, :has_title, :label_title, :disabled
  json.url record_type_url(record_type, format: :json)
end