json.array!(@taxonomies) do |taxonomy|

  json.extract! taxonomy, :public
  json.url taxonomy_url(taxonomy, format: :json)
  json.name taxonomy.record_type, :name
end