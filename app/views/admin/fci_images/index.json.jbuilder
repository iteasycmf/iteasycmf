json.array!(@fci_images) do |fci_image|
  json.extract! fci_image, :value, :alt, :title
  json.url fci_image_url(fci_image, format: :json)
end