json.array!(@fci_dates) do |fci_date|
  json.extract! fci_date, :value
  json.url fci_date_url(fci_date, format: :json)
end