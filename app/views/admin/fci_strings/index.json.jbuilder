json.array!(@fci_strings) do |fci_string|
  json.extract! fci_string, :value
  json.url fci_string_url(fci_string, format: :json)
end