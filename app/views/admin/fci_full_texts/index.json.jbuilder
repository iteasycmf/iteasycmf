json.array!(@fci_full_texts) do |fci_full_text|
  json.extract! fci_full_text, :fci_id, :mid, :model, :value
  json.url fci_full_text_url(fci_full_text, format: :json)
end