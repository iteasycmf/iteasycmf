json.array!(@filter_groups) do |filter_group|
  json.extract! filter_group, :name, :public, :position, :mid, :model, :desc
  json.url filter_group_url(filter_group, format: :json)
end