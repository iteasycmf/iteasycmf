json.array!(@field_customs) do |field_custom|
  json.extract! field_custom, :name, :machine_name, :active
  json.url field_custom_url(field_custom, format: :json)
end