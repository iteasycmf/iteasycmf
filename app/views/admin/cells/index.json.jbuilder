json.array!(@cells) do |cell|
  json.extract! cell, :record_type_id, :public, :position
  json.url cell_url(cell, format: :json)
end