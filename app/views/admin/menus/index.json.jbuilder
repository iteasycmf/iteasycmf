json.array!(@menus) do |menu|
  json.extract! menu, :name, :desc, :public, :block_id, :machine_name
  json.url menu_url(menu, format: :json)
end