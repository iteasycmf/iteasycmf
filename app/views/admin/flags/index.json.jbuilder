json.array!(@flags) do |flag|
  json.extract! flag, :user_id, :mid, :model, :type_flag, :position
  json.url flag_url(flag, format: :json)
end