json.array!(@systems) do |system|
  json.extract! system, :name, :value, :valueb, :image, :help, :category
  json.url system_url(system, format: :json)
end