json.array!(@seomores) do |seomore|
  json.extract! seomore, :title, :keywords, :desc, :noindex, :nofollow, :canonical, :author, :publisher, :alternate, :refresh
  json.url seomore_url(seomore, format: :json)
end