json.array!(@messages) do |message|
  json.extract! message, :theme, :body, :conversation_id, :message_id
  json.url message_url(message, format: :json)
end