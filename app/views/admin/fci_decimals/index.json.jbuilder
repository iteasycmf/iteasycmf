json.array!(@fci_decimals) do |fci_decimal|
  json.extract! fci_decimal, :value
  json.url fci_decimal_url(fci_decimal, format: :json)
end