json.array!(@fci_booleans) do |fci_boolean|
  json.extract! fci_boolean, :value
  json.url fci_boolean_url(fci_boolean, format: :json)
end