json.array!(@users) do |user|
  json.extract! user, :email, :admin, :edit
  json.url user_url(user, format: :json)
end