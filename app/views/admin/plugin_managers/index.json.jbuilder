json.array!(@plugin_managers) do |plugin_manager|
  json.extract! plugin_manager,
                json.url plugin_manager_url(plugin_manager, format: :json)
end