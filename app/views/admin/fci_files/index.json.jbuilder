json.array!(@fci_files) do |fci_file|
  json.extract! fci_file, :value, :fci_id, :mid, :model
  json.url fci_file_url(fci_file, format: :json)
end