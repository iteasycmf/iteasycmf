json.array!(@fcis) do |fci|
  json.extract! fci, :name, :machine_name, :active, :field_custom_id, :mid, :model, :position
  json.url fci_url(fci, format: :json)
end