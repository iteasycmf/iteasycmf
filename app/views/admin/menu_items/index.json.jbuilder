json.array!(@menu_items) do |menu_item|
  json.extract! menu_item, :name, :url, :title, :menu_id, :menu_item_id, :public, :position
  json.url menu_item_url(menu_item, format: :json)
end