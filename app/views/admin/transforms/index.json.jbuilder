json.array!(@transforms) do |transform|
  json.extract! transform, :record_type_id, :public, :seomore_id, :menu_items_id, :block, :mainform, :calc, :url
  json.url transform_url(transform, format: :json)
end