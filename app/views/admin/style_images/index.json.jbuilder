json.array!(@style_images) do |style_image|
  json.extract! style_image, :name, :machine_name, :process, :widht, :height, :percent
  json.url style_image_url(style_image, format: :json)
end