json.array!(@blocks) do |block|
  json.extract! block, :name, :region, :type_url, :url, :body, :csscls, :machine_name, :public, :position, :system
  json.url block_url(block, format: :json)
end