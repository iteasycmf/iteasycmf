json.array!(@profiles) do |profile|
  json.extract! profile, :id, :record_type_id
  json.url profile_url(profile, format: :json)
end
