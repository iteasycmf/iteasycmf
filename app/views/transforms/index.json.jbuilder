json.array!(@transforms) do |transform|
  json.extract! transform, :id, :record_type_id, :public, :seomore_id, :menu_item_id, :block, :mainform, :calc
  json.url transform_url(transform, format: :json)
end
