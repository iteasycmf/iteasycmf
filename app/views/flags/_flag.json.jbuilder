json.extract! flag, :id, :user_id, :mid, :model, :type_flag, :position, :created_at, :updated_at
json.url flag_url(flag, format: :json)