json.extract! message, :id, :theme, :body, :conversation_id, :message_id, :created_at, :updated_at
json.url message_url(message, format: :json)