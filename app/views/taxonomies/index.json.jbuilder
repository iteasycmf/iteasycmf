json.array!(@taxonomies) do |taxonomy|
  json.extract! taxonomy, :id, :public, :blocked, :url, :taxonomy_id, :position, :record_type_id
  json.url taxonomy_url(taxonomy, format: :json)
end
