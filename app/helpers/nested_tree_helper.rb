module NestedTreeHelper
  def suckerfish(items)
    items.map do |item, sub_item|
      content_tag :li, :id => dom_id(item), :class => cycle('odd', 'even') do
        render(item) + (content_tag(:ul, suckerfish(item.children)) if item.has_children?)
      end
    end.join.html_safe
  end
end