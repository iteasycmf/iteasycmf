module ApplicationHelper

  def model_url(model)
    if model.record_type.path_root_url?

        if params[:locale]
          root_url+'/'+model.url
        else
          root_url+model.url
        end
      else
        if params[:locale]
          root_url+'/'+model.entity+'/'+model.url
        else
          root_url+model.entity+'/'+model.url
        end
      end

  end

  def menu_item_url(item)
    if params[:locale]
        root_url+params[:locale]+'/'+item.url if item.url
      else
        root_url+item.url if item.url
      end
  end
  def lang_switcher
    content_tag(:ul, class: 'lang-switcher clearfix') do
      I18n.available_locales.each do |loc|
        locale_param = request.path == root_path ? root_path(locale: loc) : params.merge(locale: loc)
        if locale_param == "/" || locale_param == "/"+loc.to_s
          concat content_tag(:li, (link_to loc, locale_param), class: (I18n.locale == loc ? "active" : ""))
        else
          concat content_tag(:li, (link_to loc, locale_param.permit!), class: (I18n.locale == loc ? "active" : ""))
        end
      end
    end
  end

  def form_new
    Transform.new
  end

  def comment_new
    Comment.new
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def admin_style
    System.find_by_key(:admin_style).value
  end

  def get_menu_items(machine_name)
    suckerfish Menu.find_by_machine_name(machine_name).menu_items.includes(:translations).where(public: true).roots
  end

  def roots_menu_items(machine_name)
    Menu.find_by_machine_name(machine_name).menu_items.includes(:translations).where(public: true).roots
  end

  def admin_menu_items(machine_name)
    Menu.find_by_machine_name(machine_name).menu_items.where(public: true).roots.includes(:translations)
  end


  def theme_resolver
    if cookies.permanent[:theme] and cookies.permanent[:theme] != ''
      if params[:theme].presence.present?
        cookies.permanent[:theme] = params[:theme].presence
        cookies.permanent[:theme]
      else
        cookies.permanent[:theme]
      end
    else
      if params[:theme].presence.present?
        cookies.permanent[:theme] = params[:theme].presence
        cookies.permanent[:theme]
      else
        System.find_by_key(:theme_resolver).value
      end
    end
  end

  def theme_setting
    YAML.load_file('app/themes/'+theme_resolver+'/setting.yml')
  end

  def comments_count(model)
    Comment.where(mid: model.id, model: model.model_name.name).count
  end

  def model_each(model, name)
    model.constantize.where(entity: name, public: true)
  end

  def relation_main(model_main, model)
    rt=RecordType.find_by_machine_name(model_main)
    ModelRelation.where(model_main: rt.model, mid: model.id, model: model.model_name.name, record_type: model.entity)
  end

  def array(name)
    rt = RecordType.find_by(machine_name: name)
    @search = rt.model.constantize.where(record_type_id: rt.id).search(params[:q])
    @search.result.page params[:page]
  end

end
