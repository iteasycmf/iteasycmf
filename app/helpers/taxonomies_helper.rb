module TaxonomiesHelper

	def taxonomies_list(entity)
		Taxonomy.where(public: true, entity: entity)
	end

	def taxonomies_root(entity)
		Taxonomy.where(public: true, entity: entity).roots
	end

	def taxonomies_root_paginator(entity)
		Taxonomy.where(public: true, entity: entity).roots.page params[:page]
	end

	def taxonomies_root_paginator_ransack(entity)
		Taxonomy.where(public: true, entity: entity).roots.ransack(kartinkas_value_present: 1).result.page params[:page]
	end

	def taxonomies_child(taxonomy)
		taxonomy.children.where(public: true)
	end

end
