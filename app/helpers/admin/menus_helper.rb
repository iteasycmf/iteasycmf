module Admin::MenusHelper
  def admin_menu
    Menu.find_by_machine_name(:admin_menu)
  end
end
