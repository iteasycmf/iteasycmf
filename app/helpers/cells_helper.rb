module CellsHelper

  def cells(model, entity)
    @cells=model.cells.where(public:true, entity: entity)
    unless @cells.nil? || @cells.empty?
      if FileTest.exists?('app/themes/'+theme_resolver+'/views/cells/_'+@cells.first.entity+'.html.erb')
        render 'cells/'+@cells.first.entity
      else
        render 'cells/list_cell'
      end
    end
  end

  def group_cells(model, entity, group)
    @cells=model.cells.where(public:true, entity: entity, group: group)
    unless @cells.nil? || @cells.empty?
      if FileTest.exists?('app/themes/'+theme_resolver+'/views/cells/_'+@cells.first.entity+'.html.erb')
        render 'cells/'+@cells.first.entity
      else
        render 'cells/list_cell'
      end
    end
  end

  def cells_list(entity)
    @search = Cell.where(public: true, entity: entity).search(params[:q])
    @search.result.page params[:page]

  end

end