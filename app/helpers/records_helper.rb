module RecordsHelper
  def records_list(entity)
    Record.where(public: true, entity: entity).page params[:page]
  end

  def records_list_full(entity)
    Record.where(public: true, entity: entity)
  end

end
