module FieldsHelper

  def dynamic_field(model)
    model.fcis.collect do |namefield|
      if namefield.active?
        field(model, namefield.machine_name)
      end
    end.join.html_safe
  end

  def field(model, name)
    field=model.fcis.find_by_machine_name("#{name}")
    unless field.nil?
      @field_custom =JSON.parse(field.field_custom.value)
      field_custom = @field_custom['model'].constantize
      if ActiveRecord::Base.connection.data_source_exists? @field_custom['model'].tableize+'_translations'
        @field = field_custom.includes(:translations).find_by(fci_id: field.id, mid: model.id, model: model.model_name.human)
      else
        @field = field_custom.includes(:fci).find_by(fci_id: field.id, mid: model.id, model: model.model_name.human)
      end
      unless @field.nil?
        @fci=@field.fci
        if FileTest.exists?('app/themes/'+theme_resolver+'/views/'+@field.model.downcase+'s/'+@fci.record_type.machine_name+'/_field_'+@fci.machine_name+'.html.erb')
          render @field.model.downcase+'s/'+@fci.record_type.machine_name+'/field_'+@fci.machine_name+'.html.erb'
        else
          render @field_custom['renderfront'], locals: @field unless @field.nil?
        end
      end
    end
  end

  def field_text_short(model)
    field = FciText.where(model: model.model_name.name, mid: model.id).take
    unless field.nil?
      field.shortvalue.html_safe unless field.shortvalue.nil?
    end
  end

  def field_image(model, name, style_size)
    field=model.fcis.find_by_machine_name("#{name}")
    unless field.nil?
      @field_custom =JSON.parse(field.field_custom.value)
      #if field.multi?
        #@fields = @field_custom['model'].constantize.includes(:translations).where(fci_id: field.id, mid: model.id, model: model.model_name.human)
        #unless @fields.nil?
         # @fields.each do |field|
         #   unless field.value.nil?
         #     image_tag field.value.url(style_size), alt: field.alt, title: field.title
         #   end
         # end
        #end
      #else
        @field = @field_custom['model'].constantize.includes(:translations).find_by(fci_id: field.id, mid: model.id, model: model.model_name.human)
        unless @field.nil?
          unless @field.value.nil?
            image_tag @field.value.url(style_size), alt: @field.alt, title: @field.title
          end
        end
      #end
    end
  end

  def field_image_original_url(model, name)
    field=model.fcis.find_by_machine_name("#{name}")
    unless field.nil?
      @field_custom =JSON.parse(field.field_custom.value)
      @field = @field_custom['model'].constantize.includes(:translations).find_by(fci_id: field.id, mid: model.id, model: model.model_name.human)
      unless @field.nil?
        unless @field.value.nil?
          @field.value.url
        end
      end
    end
  end

  def field_image_style_url(model, name, style_size)
    field=model.fcis.find_by_machine_name("#{name}")
    unless field.nil?
      @field_custom =JSON.parse(field.field_custom.value)
      @field = @field_custom['model'].constantize.includes(:translations).find_by(fci_id: field.id, mid: model.id, model: model.model_name.human)
      unless @field.nil?
        unless @field.value.nil?
          @field.value.url(style_size)
        end
      end
    end
  end

  def field_image_original(model, name)
    field=model.fcis.find_by_machine_name("#{name}")
    unless field.nil?
      @field_custom =JSON.parse(field.field_custom.value)
      @field = @field_custom['model'].constantize.includes(:translations).find_by(fci_id: field.id, mid: model.id, model: model.model_name.human)
      unless @field.nil?
        unless @field.value.nil?
          image_tag @field.value.url, alt: @field.alt, title: @field.title
        end
      end
    end
  end

  def field_date(model, name, format)
    field=model.fcis.find_by_machine_name("#{name}")
    unless field.nil?
      @field = FciDate.includes(:fci).find_by(fci_id: field.id, mid: model.id, model: model.model_name.human)
      unless @field.nil?
        unless @field.value.nil?
          Russian::strftime(@field.value, format)
        end
      end
    end
  end

  def field_image_css(model, name, style_size, css)
    field=model.fcis.find_by_machine_name("#{name}")
    unless field.nil?
      @field_custom =JSON.parse(field.field_custom.value)
      @field = @field_custom['model'].constantize.includes(:translations).find_by(fci_id: field.id, mid: model.id, model: model.model_name.human)
      unless @field.nil?
        unless @field.value.nil?
          image_tag @field.value.url(style_size), alt: @field.alt, title: @field.title, class: css
        end
      end
    end
  end

  def title(model)
unless model.nil?
    rt=model.record_type
    if rt.has_title?
      unless rt.label_title.nil?
        fci=Fci.find_by(name: rt.label_title, mid: rt.id)
        unless fci.nil?
          title= FciString.includes(:translations).find_by(fci_id: fci.id, mid: model.id)
          unless title.nil?
            unless title.value.nil?
              title.value.html_safe
            end
          end
        end
      end
end
    end
  end

end