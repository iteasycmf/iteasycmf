module BlocksHelper
  def blocks(region)
    @blocks = Block.where(region: region)
    render 'shared/blocks/list', locale: @blocks
  end

  def blocks_type(type)
    rt=RecordType.includes(:translations).find_by_machine_name(type)
    Block.where(record_type_id: rt.id) unless rt.nil?
  end

  def block(name)
    @block = Block.includes(:translations).find_by(machine_name: name)
    render 'shared/blocks/show', locale: @block
  end

end