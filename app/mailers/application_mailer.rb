class ApplicationMailer < ActionMailer::Base
  default from: "office@wizard.ua"
  layout 'mailer'
end
