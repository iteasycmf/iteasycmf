class UserMailer < ApplicationMailer

  def welcome_email(user)
    @user = user
    @url = 'http://mp.iteasycode.com/users/sign_in'
    mail(to: @user.email, subject: 'Добро пожаловать на FOTOMAMAPAPA')
  end

end
