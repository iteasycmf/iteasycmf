#encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end
  def default_url
    "/admin/" + [version_name, "noimage.jpg"].compact.join('_')
  end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:  :resize_to_fit
  if StyleImage.exists?(1)
    StyleImage.all.each do |style|
      version style.machine_name.to_sym do
        if style.process.to_s == 'crop_on_corner'
	process :resize_to_fit => [style.widht, style.height]
          process style.process.to_s => "#{style.widht}x#{style.height}+0+0"
        elsif style.process.to_s == 'crop_and_centered'
          process style.process.to_sym => style.widht
        else
          process style.process.to_sym => [style.widht, style.height]
        end
      end
    end
  end
  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  #def extension_white_list
  #  %w(jpg jpeg gif png)
  #end


  def filename
    "#{model.id}_#{original_filename}" if original_filename.present?
  end

  private
  def crop_on_corner(geometry)
    manipulate! do |img|
      img.crop(geometry)
      img
    end
  end

  # Resize and crop square from Center
  def crop_and_centered(size)
    manipulate! do |image|
      if image[:width] < image[:height]
        remove = ((image[:height] - image[:width])/2).round
        image.shave("0x#{remove}")
      elsif image[:width] > image[:height]
        remove = ((image[:width] - image[:height])/2).round
        image.shave("#{remove}x0")
      end
      image.resize("#{size}x#{size}")
      image
    end
  end
  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

end
