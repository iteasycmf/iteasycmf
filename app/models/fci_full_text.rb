class FciFullText < ApplicationRecord
  belongs_to :fci
  translates :value
end
