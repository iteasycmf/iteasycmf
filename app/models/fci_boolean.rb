class FciBoolean < ApplicationRecord
  belongs_to :fci
  after_save :false

  def false
    unless self.value?
      self.value=false
    end
  end

end
