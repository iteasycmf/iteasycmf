class Comment < ApplicationRecord
  belongs_to :user, touch: true
  belongs_to :record_type
  has_many :fcis, through: :record_type
  RecordType.where(model: 'Comment').each do |rt|
    rt.fcis.includes(:field_custom).each do |fci|
      has_many fci.machine_name.pluralize.to_sym, -> { where(model: 'Comment', fci_id: fci.id) }, :foreign_key => 'mid', class_name: fci.field_custom.machine_name.singularize.camelize
    end
  end
  has_many :model_relations, -> { where model_main: 'Comment' }, :foreign_key => 'mid_main'
  default_scope { order("created_at DESC") }

  paginates_per 20
  before_save :set_entity

  def set_entity
    unless self.record_type_id.nil?
      if self.entity.nil?
        self.entity = self.record_type.machine_name
      end
    end
  end
end
