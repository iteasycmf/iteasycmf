class ModelRelation < ApplicationRecord
  belongs_to :record, :foreign_key => 'mid_main'
  belongs_to :profile, :foreign_key => 'mid_main'
  belongs_to :cell, :foreign_key => 'mid'
  belongs_to :taxonomy, :foreign_key => 'mid'
  belongs_to :recordother, foreign_key: 'mid', class_name: 'Record'

  #before_save :set_record_type

  #def set_record_type
  # rt= RecordType.find(self.model.constantize.find(self.mid).id)
  #  self.record_type=rt.machine_name
  #end

end
