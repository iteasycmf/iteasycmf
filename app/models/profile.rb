class Profile < ApplicationRecord
  has_many :users
  belongs_to :record_type
  has_many :fcis, through: :record_type
  FieldCustom.all.each do |field|
    has_many field.machine_name.to_sym, -> { where(model: 'Profile', mid: self) }, :foreign_key => 'mid',
             through: :fcis
  end
  RecordType.where(model: 'Profile').each do |rt|
    rt.fcis.includes(:field_custom).each do |fci|
      has_many fci.machine_name.pluralize.to_sym, -> { where(model: 'Profile', fci_id: fci.id) }, :foreign_key => 'mid', class_name: fci.field_custom.machine_name.singularize.camelize
    end
  end
  has_many :model_relations, -> { where(model_main: 'Profile') }, :foreign_key => 'mid_main', dependent: :destroy
  has_many :taxonomies, through: :model_relations
  has_many :cells, through: :model_relations
  paginates_per 20
  before_save :set_entity
  default_scope { order("created_at DESC") }


  def set_entity
    unless self.record_type_id.nil?
      if self.entity.nil?
        self.entity = self.record_type.machine_name
      end
    end
  end

  def title
    type = self.record_type
    if type.has_title?
      fci = type.fcis.find_by(mid: type.id, name: type.label_title)
      label=FciString.find_by(mid: self.id, model: 'Profile', fci_id: fci.id)
      label.value unless label.nil?
    end
  end

  def field(name)
    field=self.fcis.find_by_machine_name("#{name}")
    unless field.nil?
      @field_custom =JSON.parse(field.field_custom.value)
      @field = @field_custom['model'].constantize.find_by(fci_id: field.id, mid: self.id, model: 'Profile')
      unless @field.nil?
        @fci=@field.fci
        @field.value unless @field.value.nil?
      end
    end
  end


end
