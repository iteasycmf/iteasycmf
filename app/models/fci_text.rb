class FciText < ApplicationRecord
  belongs_to :fci
  translates :value, :shortvalue, :fallbacks_for_empty_translations => true
end
