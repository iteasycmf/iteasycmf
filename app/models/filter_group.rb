class FilterGroup < ApplicationRecord
  translates :name, :desc, :fallbacks_for_empty_translations => true
end
