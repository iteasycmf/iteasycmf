class FieldCustom < ApplicationRecord
  has_many :fcis

  translates :name, :value, :fallbacks_for_empty_translations => true
end
