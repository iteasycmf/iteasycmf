class UsersRole < ApplicationRecord
  belongs_to :user, touch: true
  belongs_to :role, touch: true
end
