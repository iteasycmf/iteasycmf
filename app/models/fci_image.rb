class FciImage < ApplicationRecord
  belongs_to :fci

  translates :alt, :title, :fallbacks_for_empty_translations => true
  mount_uploader :value, ImageUploader
end
