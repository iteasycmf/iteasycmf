class System < ApplicationRecord
  translates :name, :help, :category, :fallbacks_for_empty_translations => true
  default_scope { order("created_at DESC") }
end
