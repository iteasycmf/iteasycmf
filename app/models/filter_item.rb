class FilterItem < ApplicationRecord
  belongs_to :filter_group
  translates :value, :fallbacks_for_empty_translations => true
end
