class Role < ApplicationRecord
  has_many :users_roles, :dependent => :destroy
  has_many :users, :through => :users_roles
  has_many :permission_roles, :dependent => :destroy
  has_many :permissions, :through => :permission_roles
  accepts_nested_attributes_for :permission_roles

  translates :name, :fallbacks_for_empty_translations => true

  before_create :translit
  after_create :new_role

  def translit
    if self.machine_name.blank?
      @translit = Russian.translit(self.name)
      self.machine_name = @translit.parameterize("_")
    end
  end

  def new_role
    Permission.all.each do |perm|
      PermissionRole.create(role_id: self.id, permission_id: perm.id, active: false)
    end
  end


end
