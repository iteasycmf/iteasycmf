class PluginManager < ApplicationRecord
  translates :name, :desc, :fallbacks_for_empty_translations => true
  default_scope { order("created_at DESC") }
end
