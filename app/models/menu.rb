class Menu < ApplicationRecord
  belongs_to :block, dependent: :destroy, touch: true
  has_many :menu_items

  translates :name, :desc, :fallbacks_for_empty_translations => true

  before_save :translit
  after_create :new_menu_block
  default_scope { order("created_at DESC") }

  def translit
    if self.machine_name.blank?
      @translit = Russian.translit(self.name)
      self.machine_name = @translit.parameterize("_")
    end
  end

  def new_menu_block
    block=Block.create(public: true, csscls: self.machine_name, machine_name: self.machine_name, entity: :menu, name: self.name, region: '')
    self.update(block_id: block.id)
  end

end