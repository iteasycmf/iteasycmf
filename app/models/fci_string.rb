class FciString < ApplicationRecord
  belongs_to :fci

  paginates_per 14

  translates :value, :fallbacks_for_empty_translations => false
  after_save :translit_url

  def translit_url
    unless self.fci.record_type.nil?
      if self.fci.record_type.has_title?
        if self.fci.name == self.fci.record_type.label_title
          @model = self.model.constantize.find(self.mid)
          unless @model.nil?
            if @model.model_name.name == "Record" || @model.model_name.name == "Transform"
              if @model.url.nil?
                unless self.value.nil?
                  @translit = Russian.translit(self.value)
                  @model.update!(url: @translit.parameterize("_"))
                end
              end
            elsif @model.model_name.name == "Taxonomy"
              if @model.url.nil?
                unless self.value.nil?
                  name_as_url = Russian.translit(self.value).parameterize("_").gsub("-", "")
                  if @model.parent.present?
                    @model.update!(url: @model.parent.url+'/'+name_as_url)
                  else
                    @model.update!(url: name_as_url.parameterize("_"))
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  def cache_key
    super + '-' + Globalize.locale.to_s
  end

  #def self.with_translated_value(value_string)
   # with_translations(I18n.locale).where('fci_string_translations.value' => value_string)
  #end

  private

  #def self.ransackable_scopes
   # %i(with_translated_value)
  #end

end
