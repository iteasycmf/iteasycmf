class RecordType < ApplicationRecord
  has_many :fcis, -> { where(model: 'RecordType') }, :foreign_key => 'mid'
  translates :name, :desc, :help, :label_title, :url, :fallbacks_for_empty_translations => true
  System.where(machine_category: :record_type).each do |model|
    has_many model.key.to_sym
  end
  before_update :updatetitle
  after_create :newtitle
  after_save :reload_routes
  before_create :translit
  default_scope { order("created_at DESC") }
  paginates_per 20


  def updatetitle
    if self.has_title?
      unless self.label_title.blank?
        @was_label=RecordType::Translation.find_by_record_type_id(self.id).label_title_was
        @efci = Fci.find_by(name: @was_label)
        if @efci.nil?
          @fci = Fci.new
          @fci.name = self.label_title
          @fci.active = true
          @fci.required_field = 1
          @fci.field_custom_id = FieldCustom.find_by_machine_name(:fci_strings).id
          @fci.mid = self.id
          @fci.model = self.model_name.name
          @fci.save
        else
          @efci.update(name: self.label_title)
        end
      end
    end
  end

  def newtitle
    if self.has_title?
      unless self.label_title.blank?
        @fci = Fci.new
        @fci.name = self.label_title
        @fci.active = true
        @fci.required_field = 1
        @fci.field_custom_id = FieldCustom.find_by_machine_name(:fci_strings).id
        @fci.mid = self.id
        @fci.model = self.model_name.name
        @fci.save
      end
    end
  end

  def reload_routes
    DynamicRouter.reload
  end

  def translit
    if self.machine_name.blank?
      @translit = Russian.translit(self.name)
      self.machine_name = @translit.parameterize("_")
    end
  end

end

