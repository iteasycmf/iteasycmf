class AdminAbility
  include CanCan::Ability


  def initialize(user)

    user ||= User.new # guest user (not logged in)

    alias_action :create, :read, :update, :destroy, :to => :crud

    if user.nil?
      Role.find(3).permission_roles.joins(:permission).where(:permissions => {admin: true}).each do |permissionrole|
        if permissionrole.active?
          if permissionrole.subject_id.nil?
            can permissionrole.permission.action.to_sym, permissionrole.permission.subject_class.constantize
          else
            can permissionrole.permission.action.to_sym, permissionrole.permission.subject_class.constantize, :id => permissionrole.subject_id
          end
        end
      end
    else
      user.roles.each do |role|
        role.permissions.where(admin: true).each do |permission|
          dani = permission.permission_roles.find_by(role_id: role.id)
          if dani.active?
            if dani.subject_id.nil?
              can permission.action.to_sym, permission.subject_class.constantize
            else
              can permission.action.to_sym, permission.subject_class.constantize, :id => dani.subject_id
            end
          end
        end
      end
    end
  end
end