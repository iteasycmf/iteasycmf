class Seomore < ApplicationRecord
  has_many :records
  has_many :taxonomies
  translates :title, :keywords, :desc, :canonical,
             :author, :publisher, :alternate, :refresh, :fallbacks_for_empty_translations => true
  default_scope { order("created_at DESC") }
end
