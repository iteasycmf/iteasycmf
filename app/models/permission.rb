class Permission < ApplicationRecord
  has_many :permission_roles, :dependent => :destroy
  has_many :roles, :through => :permission_roles
  translates :name, :description, :fallbacks_for_empty_translations => true
  default_scope { order("created_at ASC") }
  after_create :new_permission

  def new_permission
    Role.all.each do |role|
      find_permission_role = role.permissions.where(subject_class: self.subject_class, action: self.action)
      unless find_permission_role.nil?
        if role.id == 1
          PermissionRole.create(role_id: role.id, permission_id: self.id, active: true)
        else
          unless self.admin?
            if self.action == 'read'
              PermissionRole.create(role_id: role.id, permission_id: self.id, active: true)
            else
              PermissionRole.create(role_id: role.id, permission_id: self.id, active: false)
            end
          else
            PermissionRole.create(role_id: role.id, permission_id: self.id, active: false)
          end
        end
      end
    end
  end
end
