class Block < ApplicationRecord
  has_many :menus
  belongs_to :record_type
  has_many :fcis, through: :record_type

  RecordType.where(model: 'Block').each do |rt|
    rt.fcis.includes(:field_custom).each do |fci|
      has_many fci.machine_name.pluralize.to_sym, -> { where(model: 'Block', fci_id: fci.id) }, :foreign_key => 'mid', class_name: fci.field_custom.machine_name.singularize.camelize
    end
  end
  
  has_many :model_relations, -> { where(model_main: 'Block') }, :foreign_key => 'mid_main', dependent: :destroy
  has_many :cells, through: :model_relations
  translates :name, :fallbacks_for_empty_translations => true
  paginates_per 20
  default_scope { order("position ASC") }

  before_save :set_entity
  before_save :translit

  def set_entity
    unless self.record_type_id.nil?
      self.entity=self.record_type.machine_name if self.entity.nil?
    end
  end

  def title
    type = self.record_type
    if type.has_title?
      fci = type.fcis.find_by(mid: type.id, name: type.label_title)
      label=FciString.find_by(mid: self.id, model: 'Block', fci_id: fci.id)
      label.value unless label.nil?
    end
  end

  def translit
    if self.machine_name.blank?
      @translit = Russian.translit(self.name)
      self.machine_name = @translit.parameterize("_")
    end
  end


end