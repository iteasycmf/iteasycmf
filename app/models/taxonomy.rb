class Taxonomy < ApplicationRecord
  has_ancestry cache_depth: true
  belongs_to :record_type
  belongs_to :seomore, touch: true
  has_many :fcis, through: :record_type
  RecordType.where(model: 'Taxonomy').each do |rt|
    rt.fcis.includes(:field_custom).each do |fci|
      has_many fci.machine_name.pluralize.to_sym, -> { where(model: 'Taxonomy', fci_id: fci.id) }, :foreign_key => 'mid', class_name: fci.field_custom.machine_name.singularize.camelize
    end
  end
  has_many :model_relations, -> { where model_main: 'Taxonomy' }, :foreign_key => 'mid_main', dependent: :destroy
  has_many :reverse_model_relations, -> { where model: 'Taxonomy' }, :foreign_key => 'mid', class_name: "ModelRelation", dependent: :destroy
  has_many :records, through: :reverse_model_relations
  extend FriendlyId
  friendly_id :url, :use => [:finders]
  translates :url, :fallbacks_for_empty_translations => true

  paginates_per 20
  default_scope { order("position ASC") }
  before_save :set_entity

  def set_entity
    unless self.record_type_id.nil?
      if self.entity.nil?
        self.entity = self.record_type.machine_name
      end
    end
  end

  def title
    type = self.record_type
    if type.has_title?
      fci = type.fcis.find_by(mid: type.id, name: type.label_title)
      label=FciString.find_by(mid: self.id, model: 'Taxonomy', fci_id: fci.id)
      label.value unless label.nil?
    end
  end

  private

  def ransackable_scopes
    %w(title)
  end

end