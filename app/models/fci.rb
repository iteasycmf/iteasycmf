class Fci < ApplicationRecord
  belongs_to :field_custom
  belongs_to :record_type, :foreign_key => 'mid'
  FieldCustom.all.each do |field|
    has_many field.machine_name.to_sym
  end
  translates :name, :value, :placeholder, :prefix, :suffix, :help, :fallbacks_for_empty_translations => true
  default_scope { order("position ASC") }
  before_save :translit
  before_destroy :delete_all

  def translit
    if self.machine_name.blank?
      @translit = Russian.translit(self.name)
      self.machine_name = @translit.parameterize("_")
    end
  end

  def delete_all
    fc = JSON.parse(self.field_custom.value)
    fc['model'].constantize.where(fci_id: self.id).map { |f| f.destroy }
  end

end