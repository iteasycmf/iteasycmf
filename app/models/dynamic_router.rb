class DynamicRouter
  def self.load
    if ActiveRecord::Base.connection.table_exists? 'systems'
      Rails.application.routes.draw do
        RecordType.all.each do |rt|
          if ActiveRecord::Base.connection.column_exists?(:record_types, :path_root_url)
            if rt.path_root_url
              if rt.model == 'Taxonomy'
                 get '/*id', :to => "#{rt.model.downcase.tableize}#show", :constraints => lambda { |r| Taxonomy.friendly.find_by(entity: rt.machine_name.downcase, url: r.params[:id]).present? }
              else
                get '/:id', :to => "#{rt.model.downcase.tableize}#show", :constraints => lambda { |r| rt.model.constantize.friendly.find_by(entity: rt.machine_name.downcase, url: r.params[:id]).present? }
              end
            else
              get "/#{rt.machine_name}/:id", :to => "#{rt.model.downcase.tableize}#show"
            end
          end
        end
      end
    end
  end

  def self.reload
    Rails.application.routes_reloader.reload!
  end
end
