class StyleImage < ApplicationRecord
  after_save :recreate_style

  def recreate_style
    FciImage.all.each do |image|
      image.value.recreate_versions!(self.machine_name.to_sym) if image.value?
    end
  end
end
