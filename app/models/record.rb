class Record < ApplicationRecord
  belongs_to :record_type
  belongs_to :seomore, touch: true
  belongs_to :menu_item, touch: true
  has_many :fcis, through: :record_type
  RecordType.where(model: 'Record').includes(:fcis).each do |rt|
    rt.fcis.includes(:field_custom).each do |fci|
      has_many fci.machine_name.pluralize.to_sym, -> {where(model: 'Record', fci_id: fci.id)}, :foreign_key => 'mid', class_name: JSON.parse(fci.field_custom.value)['model']
    end
  end
  has_many :model_relations, -> {where(model_main: 'Record')}, :foreign_key => 'mid_main', dependent: :destroy
  has_many :taxonomies, through: :model_relations
  has_many :cells, through: :model_relations
  has_many :flags, -> {where(model: 'Record')}, :foreign_key => 'mid', dependent: :destroy
  accepts_nested_attributes_for :seomore, allow_destroy: true

  translates :url, :fallbacks_for_empty_translations => true
  extend FriendlyId
  friendly_id :url, :use => [:finders]

  paginates_per 14
  default_scope {order("created_at DESC")}

  before_save :set_entity
  after_save :urlsave


  def set_entity
    unless self.record_type_id.nil?
      if self.entity.nil?
        self.entity = self.record_type.machine_name
      end
    end
  end

  def taxonoms
    self.model_relations.where(mid_main: self.id).join(:taxonomy)
  end

  def urlsave
    self.url=self.id unless self.record_type.has_title?
  end

  def title
    type = self.record_type
    if type.has_title?
      fci = type.fcis.find_by(mid: type.id, name: type.label_title)
      label=FciString.includes(:translations).find_by(mid: self.id, model: 'Record', fci_id: fci.id)
      label.value unless label.nil?
    end
  end

  def cache_key
    super + '-' + Globalize.locale.to_s
  end


end
