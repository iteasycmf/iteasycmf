class Cell < ApplicationRecord
  belongs_to :record_type
  has_many :fcis, through: :record_type

  RecordType.where(model: 'Cell').each do |rt|
    rt.fcis.includes(:field_custom).each do |fci|
      has_many fci.machine_name.pluralize.to_sym, -> { where(model: 'Cell', fci_id: fci.id) }, :foreign_key => 'mid', class_name: fci.field_custom.machine_name.singularize.camelize
    end
  end
  has_many :model_relations, -> { where model_main: 'Cell' }, :foreign_key => 'mid_main'
  has_many :reverse_model_relations, -> { where model: 'Cell' }, :foreign_key => 'mid', class_name: "ModelRelation", dependent: :destroy
  has_many :records, through: :reverse_model_relations
  has_many :blocks, through: :reverse_model_relations
  default_scope { order("position ASC") }
  paginates_per 14
  before_save :set_entity

  def set_entity
    unless self.record_type_id.nil?
      if self.entity.nil?
        self.entity = self.record_type.machine_name
      end
    end
  end

  def title
    type = self.record_type
    if type.has_title?
      fci = type.fcis.find_by(mid: type.id, name: type.label_title)
      label=FciString.includes(:translations).find_by(mid: self.id, model: 'Cell', fci_id: fci.id)
      label.value unless label.nil?
    end
  end

end