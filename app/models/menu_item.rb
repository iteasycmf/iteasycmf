class MenuItem < ApplicationRecord
  has_ancestry
  belongs_to :menu, touch: true
  belongs_to :menu_item, touch: true
  default_scope { order("position ASC") }
  translates :name, :title, :url, :fallbacks_for_empty_translations => true
end