class User < ApplicationRecord
  TEMP_EMAIL_PREFIX = 'change@me'
  TEMP_EMAIL_REGEX = /\Achange@me/
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable, :authentication_keys => [:login]

  has_many :users_roles, dependent: :destroy
  has_many :roles, :through => :users_roles
  has_many :model_settings, dependent: :destroy
  belongs_to :profile, dependent: :destroy, touch: true
  has_many :comments, dependent: :destroy
  has_many :flags
  has_many :conversations
  has_many :chat_rooms, dependent: :destroy
  has_many :messages, dependent: :destroy

  attr_accessor :login
  before_create :create_role
  before_save :admincheck

  validates_format_of :email, :without => TEMP_EMAIL_REGEX, on: :update


  default_scope { order("created_at DESC") }

  validate :validate_username

  def validate_username
    if username.present?
      if User.where(email: username).exists?
        errors.add(:username, :invalid)
      end
    end
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      if conditions[:username].nil?
        where(conditions).first
      else
        where(username: conditions[:username]).first
      end
    end
  end

  def admincheck
    self.username = self.id
    roles.each do |role|
      if role.machine_name == 1
        self.admin = true
      end
    end

  end

  def email_verified?
    self.email && self.email !~ TEMP_EMAIL_REGEX
  end


  def self.current
    Thread.current[:user]
  end

  def self.current=(user)
    Thread.current[:user] = user
  end

  def email_required?
    true
  end

  def password_required?
    new_record? ? false : super
  end

  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end


  private
  def create_role
    self.roles << Role.find_by_machine_name(:user)

  end

end
