class Transform < ApplicationRecord
  belongs_to :record_type
  has_many :fcis, through: :record_type
  belongs_to :seomore, touch: true
  belongs_to :menu_item, touch: true
  RecordType.where(model: 'Transform').each do |rt|
    rt.fcis.includes(:field_custom).each do |fci|
      has_many fci.machine_name.pluralize.to_sym, -> { where(model: 'Transform', fci_id: fci.id) }, :foreign_key => 'mid', class_name: fci.field_custom.machine_name.singularize.camelize
    end
  end

  translates :url, :fallbacks_for_empty_translations => true
  extend FriendlyId
  friendly_id :url, :use => [:finders]

  default_scope { order("created_at DESC") }

end
