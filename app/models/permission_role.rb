class PermissionRole < ApplicationRecord
  belongs_to :role, touch: true
  belongs_to :permission, touch: true
  default_scope { order("created_at ASC") }
end