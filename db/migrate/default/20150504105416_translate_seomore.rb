class TranslateSeomore < ActiveRecord::Migration
  def self.up
    Seomore.create_translation_table!({
                                          :title => :string, :keywords => :string, :desc => :text, :canonical => :string,
                                          :author => :string, :publisher => :string, :alternate => :string, :refresh => :string
                                      }, {
                                          :migrate_data => true
                                      })
  end

  def self.down
    Seomore.drop_translation_table! :migrate_data => true
  end
end
