class CreateCells < ActiveRecord::Migration
  def change
    create_table :cells do |t|
      t.references :record_type, index: true, foreign_key: true
      t.boolean :public
      t.integer :position

      t.timestamps null: false
    end
  end
end
