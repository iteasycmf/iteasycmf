class AddValueSystemSeo < ActiveRecord::Migration
  def up
    I18n.locale = :en
    System.create([{name: 'Robots value', key: :robotstxt, blocked: true},
                   {key: :xmltitle, name: 'Feeds title', machine_category: :feeds, blocked: true},
                   {key: :xmldescription, name: 'Feeds description', machine_category: :feeds, blocked: true}])
  end
end
