class AddExemplarToRecordAndOther < ActiveRecord::Migration
  def change
    add_column :blocks, :entity, :string
    add_column :records, :entity, :string
    add_column :profiles, :entity, :string
    add_column :taxonomies, :entity, :string
    add_column :comments, :entity, :string
    add_column :cells, :entity, :string
  end
end
