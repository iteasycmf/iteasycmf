class AddFieldsToModelSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :model_settings, :value, :text
    add_column :model_settings, :mid, :integer
    add_column :model_settings, :model, :string
  end
end
