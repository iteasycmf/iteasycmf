class TranslateBlocks < ActiveRecord::Migration
  def self.up
    Block.create_translation_table!({
                                        :name => :string
                                    }, {
                                        :migrate_data => true
                                    })
  end

  def self.down
    Block.drop_translation_table! :migrate_data => true
  end
end
