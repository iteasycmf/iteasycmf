class AddUserToModelSettings < ActiveRecord::Migration[5.0]
  def change
    add_reference :model_settings, :user, foreign_key: true
  end
end
