class CreateSystems < ActiveRecord::Migration
  def change
    create_table :systems do |t|
      t.string :key
      t.string :value
      t.boolean :valueb
      t.string :image
      t.string :machine_category
      t.boolean :blocked
      t.boolean :hidden
      t.integer :position

      t.timestamps null: false
    end
  end
end
