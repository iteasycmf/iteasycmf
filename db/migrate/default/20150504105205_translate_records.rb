class TranslateRecords < ActiveRecord::Migration
  def self.up
    Record.create_translation_table!({
                                         :url => :string
                                     }, {
                                         :migrate_data => true
                                     })
  end

  def self.down
    Record.drop_translation_table! :migrate_data => true
  end
end
