class CreateModelSettings < ActiveRecord::Migration
  def change
    create_table :model_settings do |t|
      t.integer :position
      t.string :ancestry

      t.timestamps null: false
    end
  end
end
