class AddField2ToFci < ActiveRecord::Migration
  def change
    add_column :fcis, :multi, :boolean
    add_column :fcis, :elementfield, :string
    add_column :fcis, :classfield, :string
    add_column :fcis, :idfield, :string
  end
end
