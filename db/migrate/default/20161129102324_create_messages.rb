class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.string :theme
      t.text :body
      t.references :conversation, foreign_key: true
      t.references :message, foreign_key: true

      t.timestamps
    end
  end
end
