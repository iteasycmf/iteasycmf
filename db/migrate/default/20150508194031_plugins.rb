class Plugins < ActiveRecord::Migration
  def change
    create_table :plugin_managers do |t|
      t.boolean :active
      t.string :machine_name
      t.string :app_path
      t.boolean :blocked
      t.string :remote_path

      t.timestamps null: false
    end
  end
end
