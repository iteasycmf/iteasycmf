class TranslateMenuItems < ActiveRecord::Migration
  def self.up
    MenuItem.create_translation_table!({
                                           :name => :string, :title => :string, :url => :string
                                       }, {
                                           :migrate_data => true
                                       })
  end

  def self.down
    MenuItem.drop_translation_table! :migrate_data => true
  end
end
