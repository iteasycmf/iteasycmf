class AddFieldToFci < ActiveRecord::Migration
  def up
    Fci.add_translation_fields! prefix: :string, suffix: :string
  end

  def down
    remove_column :fci_translations, :prefix, :suffix
  end
end
