class CreateRecordTypes < ActiveRecord::Migration
  def change
    create_table :record_types do |t|
      t.string :machine_name
      t.boolean :has_title
      t.boolean :disabled
      t.string :model
      t.boolean :blocked
      t.boolean :position
      t.string :comment_type
      t.boolean :nesting
      t.boolean :relation

      t.timestamps null: false
    end
  end
end
