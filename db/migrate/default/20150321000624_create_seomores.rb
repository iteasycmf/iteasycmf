class CreateSeomores < ActiveRecord::Migration
  def change
    create_table :seomores do |t|
      t.boolean :noindex
      t.boolean :nofollow

      t.timestamps null: false
    end
  end
end
