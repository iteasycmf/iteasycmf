class Flag < ActiveRecord::Migration[5.0]
  def up
    create_table :flags do |t|
      t.string :type_flag
      t.integer :mid
      t.string :model
	    t.references :user, index: true, foreign_key: true
      t.integer :position

      t.timestamps
    end
  end

  def down
     drop_table :flags
  end
end
