class TranslateFcis < ActiveRecord::Migration
  def self.up
    Fci.create_translation_table!({
                                      :name => :string, :placeholder => :string, :value => :text
                                  }, {
                                      :migrate_data => true
                                  })
  end

  def self.down
    Fci.drop_translation_table! :migrate_data => true
  end
end
