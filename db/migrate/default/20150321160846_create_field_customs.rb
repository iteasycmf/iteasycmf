class CreateFieldCustoms < ActiveRecord::Migration
  def change
    create_table :field_customs do |t|
      t.string :machine_name
      t.boolean :active

      t.timestamps null: false
    end
  end
end
