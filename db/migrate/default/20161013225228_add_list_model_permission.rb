class AddListModelPermission < ActiveRecord::Migration[5.0]
  def up
    %w( Record Taxonomy Comment Transform).each do |subject|
      %w( list ).each do |action|
        Permission.create(subject_class: subject, action: action, admin: true, name: action+" to "+subject)
      end
    end

    I18n.locale = :ru
    Permission.find_by(subject_class: "Record", action: :list, admin: true).update(name: "Чтение списка записей",description: "Доступ к странице списков записи. Ко всем записям, не разделенным по типам")
    Permission.find_by(subject_class: "Taxonomy", action: :list, admin: true).update(name: "Чтение списка категорий",description: "Доступ к странице списков таксономии. Ко всем таксономиям, не разделенным по типам")
    Permission.find_by(subject_class: "Comment", action: :list, admin: true).update(name: "Чтение списка комментариев",description: "Доступ к странице списков комментариев. Ко всем комментариям, не разделенным по типам")
    Permission.find_by(subject_class: "Transform", action: :list, admin: true).update(name: "Чтение списка форм",description: "Доступ к странице списков форм. Ко всем формам, не разделенным по типам")
  end
end
