class TranslateFieldCustoms < ActiveRecord::Migration
  def self.up
    FieldCustom.create_translation_table!({
                                              :name => :string, :value => :text
                                          }, {
                                              :migrate_data => true
                                          })
  end

  def self.down
    FieldCustom.drop_translation_table! :migrate_data => true
  end
end
