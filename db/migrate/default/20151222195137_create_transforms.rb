class CreateTransforms < ActiveRecord::Migration

  def up
    create_table :transforms do |t|
      t.references :record_type, index: true, foreign_key: true
      t.boolean :public
      t.references :seomore, index: true, foreign_key: true
      t.references :menu_item, index: true, foreign_key: true
      t.boolean :block
      t.boolean :mainform
      t.boolean :calc

      t.timestamps null: false
    end
    Transform.create_translation_table! :url => :string
  end

  def down
    drop_table :transforms
    Transform.drop_translation_table!
  end

end
