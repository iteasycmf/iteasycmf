class AddRelationCell < ActiveRecord::Migration[5.0]
  def up
  	add_column :fci_full_texts, :format_field, :string
  	add_column :fci_texts, :format_field, :string
  	Fci.add_translation_fields! help: :text
  end
  def down
  	remove_column :fci_full_texts, :format_field, :string
  	remove_column :fci_texts, :format_field, :string
  	remove_column :fci_translations, :help
  end
end
