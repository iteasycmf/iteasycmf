class AddAncestryDepthToTaxonomy < ActiveRecord::Migration[5.0]
  def change
    add_column :taxonomies, :ancestry_depth, :integer, :default => 0
  end
end
