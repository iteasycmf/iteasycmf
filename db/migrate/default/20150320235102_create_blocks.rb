class CreateBlocks < ActiveRecord::Migration
  def change
    create_table :blocks do |t|
      t.string :region
      t.string :type_url
      t.text :url
      t.string :csscls
      t.string :machine_name
      t.boolean :public
      t.integer :position
      t.boolean :system
      t.references :record_type, index: true, foreign_key: true
      t.boolean :blocked

      t.timestamps null: false
    end
  end
end
