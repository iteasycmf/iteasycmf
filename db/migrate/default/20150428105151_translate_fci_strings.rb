class TranslateFciStrings < ActiveRecord::Migration
  def self.up
    FciString.create_translation_table!({
                                            :value => :string
                                        }, {
                                            :migrate_data => true
                                        })
  end

  def self.down
    FciString.drop_translation_table! :migrate_data => true
  end
end
