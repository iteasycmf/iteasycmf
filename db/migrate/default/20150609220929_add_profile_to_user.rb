class AddProfileToUser < ActiveRecord::Migration
  def up
    I18n.locale = :en
    RecordType.create(name: 'Personal', desc: 'Adding personal pages to user', has_title: true, label_title: 'Name',
                      url: :personal, disabled: false, machine_name: :personals, blocked: true, model: 'Profile')
  end
end
