class TranslateFciImages < ActiveRecord::Migration
  def self.up
    FciImage.create_translation_table!({
                                           :alt => :string, :title => :string
                                       }, {
                                           :migrate_data => true
                                       })
  end

  def self.down
    FciImage.drop_translation_table! :migrate_data => true
  end
end
