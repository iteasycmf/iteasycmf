class CreateFcis < ActiveRecord::Migration
  def change
    create_table :fcis do |t|
      t.string :machine_name
      t.boolean :active
      t.string :required_field
      t.boolean :blocked
      t.references :field_custom, index: true, foreign_key: true
      t.integer :mid
      t.string :model
      t.integer :position

      t.timestamps null: false
    end
  end
end