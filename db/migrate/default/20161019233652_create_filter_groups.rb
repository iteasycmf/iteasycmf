class CreateFilterGroups < ActiveRecord::Migration[5.0]
  def up
    create_table :filter_groups do |t|
      t.boolean :public
      t.integer :position
      t.integer :mid
      t.string :model

      t.timestamps
    end
    FilterGroup.create_translation_table! :name => :string, :desc => :text
  end

  def down
     drop_table :filter_groups
     FilterGroup.drop_translation_table!
  end
end
