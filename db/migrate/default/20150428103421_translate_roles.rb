class TranslateRoles < ActiveRecord::Migration
  def self.up
    Role.create_translation_table!({
                                       :name => :string
                                   }, {
                                       :migrate_data => true
                                   })
  end

  def self.down
    Role.drop_translation_table! :migrate_data => true
  end
end
