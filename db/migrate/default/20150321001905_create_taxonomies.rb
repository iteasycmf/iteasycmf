class CreateTaxonomies < ActiveRecord::Migration
  def change
    create_table :taxonomies do |t|
      t.string :ancestry
      t.boolean :public
      t.integer :position
      t.references :record_type, index: true, foreign_key: true
      t.boolean :blocked
      t.references :seomore, index: true, foreign_key: true
      t.references :menu_item, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end