class DelColumnPermissions < ActiveRecord::Migration
  def change
    remove_column :permissions, :role_id, :integer
    remove_column :permissions, :subject_id, :integer
    remove_column :permissions, :active, :boolean
  end
end
