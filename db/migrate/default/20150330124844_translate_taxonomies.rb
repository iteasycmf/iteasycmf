class TranslateTaxonomies < ActiveRecord::Migration
  def self.up
    Taxonomy.create_translation_table!({
                                           :url => :string
                                       }, {
                                           :migrate_data => true
                                       })
  end

  def self.down
    Taxonomy.drop_translation_table! :migrate_data => true
  end
end