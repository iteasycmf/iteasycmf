class ExemplarToNilBlock < ActiveRecord::Migration
  def up
    Block.all.each do |r|
      if r.record_type_id.nil?
        r.update(entity: 'system')
      end
    end
  end
end
