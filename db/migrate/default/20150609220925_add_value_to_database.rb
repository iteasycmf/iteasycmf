class AddValueToDatabase < ActiveRecord::Migration
  def up
    I18n.locale = :en
    Role.create(:machine_name => :admin, :name => :admin, blocked: true)
    Role.create(:machine_name => :user, :name => :user, blocked: true)
    Role.create(:machine_name => :guest, :name => :guest, blocked: true)

    System.create([{name: :admin_style, key: :admin_style, value: :darkblue, blocked: true},
                   {name: :locale, key: :locale, value: :en, blocked: true},
                   {name: :localeroute, key: :localeroute, value: 'en|ru', blocked: true},
                   {key: 'blocks', name: 'Block', machine_category: :record_type, value: 'Block', blocked: true, hidden: true},
                   {key: 'records', name: 'Record', machine_category: :record_type, value: 'Record', blocked: true, hidden: true},
                   {key: 'profiles', name: 'Profile', machine_category: :record_type, value: 'Profile', blocked: true, hidden: true},
                   {key: 'taxonomies', name: 'Taxonomy', machine_category: :record_type, value: 'Taxonomy', blocked: true, hidden: true},
                   {key: 'comments', name: 'Comment', machine_category: :record_type, value: 'Comment', blocked: true, hidden: true},
                   {key: 'cells', name: 'Cell', machine_category: :record_type, value: 'Cell', blocked: true, hidden: true},
                   {key: 'theme_resolver', name: 'Theme', machine_category: :theme, value: :iteasycmf, blocked: true}])
    FieldCustom.create([{name: :string, machine_name: :fci_strings, active: true, value: '{"render":"admin/fci_strings/form_field",
 "model":"FciString", "param":[{"line":"value", "value":"fci_string_value"}], "renderfront":"shared/fields/fci_string"}'},
                        {name: :text, machine_name: :fci_texts, active: true, value: '{"render":"admin/fci_texts/form_field",
 "model":"FciText", "param":[{"line":"value", "value":"fci_text_value"}, {"line":"shortvalue", "value":"fci_shorttext_value"}],
 "renderfront":"shared/fields/fci_text"}'},
                        {name: :decimal, machine_name: :fci_decimals, active: true, value: '{"render":"admin/fci_decimals/form_field",
"model":"FciDecimal", "param":[{"line":"value", "value":"fci_decimal_value"}], "renderfront":"shared/fields/fci_decimal"}'},
                        {name: :boolean, machine_name: :fci_booleans, active: true, value: '{"render":"admin/fci_booleans/form_field",
 "model":"FciBoolean", "param":[{"line":"value", "value":"fci_boolean_value"}], "renderfront":"shared/fields/fci_boolean"}'},
                        {name: :date, machine_name: :fci_dates, active: true, value: '{"render":"admin/fci_dates/form_field",
"model":"FciDate", "param":[{"line":"value", "value":"fci_date_value"}], "renderfront":"shared/fields/fci_date"}'},
                        {name: :image, machine_name: :fci_images, active: true, value: '{"render":"admin/fci_images/form_field",
"model":"FciImage", "param":[{"line":"value", "value":"fci_image_value"}, {"line":"alt", "value":"fci_alt_value"},
{"line":"title", "value":"fci_title_value"}], "renderfront":"shared/fields/fci_image"}'},
                        {name: :file, machine_name: :fci_files, active: true, value: '{"render":"admin/fci_files/form_field",
"model":"FciFile", "param":[{"line":"value", "value":"fci_file_value"}], "renderfront":"shared/fields/fci_file"}'},
                        {name: :integer, machine_name: :fci_integers, active: true, value: '{"render":"admin/fci_integers/form_field",
"model":"FciInteger", "param":[{"line":"value", "value":"fci_integer_value"}], "renderfront":"shared/fields/fci_integer"}'},
                        {name: 'full text', machine_name: :fci_full_texts, active: true, value: '{"render":"admin/fci_full_texts/form_field",
"model":"FciFullText", "param":[{"line":"value", "value":"fci_full_text_value"}], "renderfront":"shared/fields/fci_full_text"}'},
                        {name: 'list text', machine_name: :fci_list_texts, active: true, value: '{"render":"admin/fci_strings/list_texts_form_field",
"model":"FciString", "param":[{"line":"value", "value":"fci_list_text_value"}], "renderfront":"shared/fields/fci_list_text"}'},
                        {name: 'list integer', machine_name: :fci_list_integers, active: true, value: '{"render":"admin/fci_integers/list_integers_form_field",
"model":"FciInteger", "param":[{"line":"value", "value":"fci_list_integer_value"}], "renderfront":"shared/fields/fci_list_integer"}'},
                        {name: 'list decimal', machine_name: :fci_list_decimals, active: true, value: '{"render":"admin/fci_decimals/list_decimals_form_field",
"model":"FciDecimal", "param":[{"line":"value", "value":"fci_list_decimal_value"}], "renderfront":"shared/fields/fci_list_decimal"}'},
                        {name: 'email', machine_name: :fci_emails, active: true, value: '{"render":"admin/fci_strings/emails_form_field",
"model":"FciString", "param":[{"line":"value", "value":"fci_email_value"}], "renderfront":"shared/fields/fci_email"}'},
                        {name: 'phone', machine_name: :fci_phones, active: true, value: '{"render":"admin/fci_strings/phones_form_field",
"model":"FciString", "param":[{"line":"value", "value":"fci_phone_value"}], "renderfront":"shared/fields/fci_phone"}'},
                        {name: 'url', machine_name: :fci_urls, active: true, value: '{"render":"admin/fci_strings/urls_form_field",
"model":"FciString", "param":[{"line":"value", "value":"fci_url_value"}], "renderfront":"shared/fields/fci_url"}'},
                        {name: 'color', machine_name: :fci_colors, active: true, value: '{"render":"admin/fci_strings/colors_form_field",
"model":"FciString", "param":[{"line":"value", "value":"fci_color_value"}], "renderfront":"shared/fields/fci_color"}'},
                        {name: 'reference', machine_name: :fci_references, active: true, value: '{"render":"admin/fci_references/form_field",
"model":"FciReference", "param":[{"line":"value", "value":"fci_reference_value"}], "renderfront":"shared/fields/fci_reference"}'}])

    StyleImage.create([{name: :thumb, machine_name: :thumb, height: 100, widht: 100, blocked: true, process: :resize_to_fit},
                       {name: :medium, machine_name: :medium, height: 250, widht: 250, blocked: true, process: :resize_to_fit},
                       {name: :large, machine_name: :large, height: 450, widht: 550, blocked: true, process: :resize_to_fit}])
    RecordType.create(name: 'Pages', desc: 'Adding standard pages with a title and content', has_title: true, label_title: 'Title',
                      url: :pages, disabled: false, machine_name: :pages, blocked: true, model: 'Record')

    User.create(email: "admin@admin.com", password: "admin1234", edit: true, admin: true)
    UsersRole.create(user_id: 1, role_id: 1)

  end
end
