class TranslateSystems < ActiveRecord::Migration
  def self.up
    System.create_translation_table!({
                                         :name => :string, :help => :text, :category => :string
                                     }, {
                                         :migrate_data => true
                                     })
  end

  def self.down
    System.drop_translation_table! :migrate_data => true
  end
end
