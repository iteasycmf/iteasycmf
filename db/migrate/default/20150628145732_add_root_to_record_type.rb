class AddRootToRecordType < ActiveRecord::Migration
  def up
    add_column :record_types, :path_root_url, :boolean
  end
end
