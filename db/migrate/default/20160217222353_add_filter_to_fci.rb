class AddFilterToFci < ActiveRecord::Migration
  def change
    add_column :fcis, :filter, :boolean
  end
end
