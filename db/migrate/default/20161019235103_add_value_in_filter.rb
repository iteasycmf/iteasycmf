class AddValueInFilter < ActiveRecord::Migration[5.0]
  def up
    FieldCustom.find_by_machine_name(:fci_references).update(value: '{"render":"admin/fci_references/form_field",
"model":"FciReference", "param":[{"line":"rmodel", "value":"fci_reference_rmodel"}, {"line":"rmid", "value":"fci_reference_rmid"}, {"line":"rrecord_type", "value":"fci_reference_rrecord_type"}], "renderfront":"shared/fields/fci_reference"}')
    %w( FilterGroup).each do |subject|
      %w( create read update destroy ).each do |action|
        Permission.create(subject_class: subject, action: action, admin: true, name: action+" to "+subject)
      end
    end
    MenuItem.create([{name: 'Group Filters', url: 'admin/filter_groups', menu_id: 1, public: true, position: 11, csscls: 'icon-shuffle'}])
  end
end
