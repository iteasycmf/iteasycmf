class CreateFciImages < ActiveRecord::Migration
  def change
    create_table :fci_images do |t|
      t.string :value
      t.references :fci, index: true, foreign_key: true
      t.integer :mid
      t.string :model

      t.timestamps null: false
    end
  end
end
