class CreatePermissionRoles < ActiveRecord::Migration
  def change
    create_table :permission_roles do |t|
      t.references :role, index: true, foreign_key: true
      t.integer :subject_id
      t.references :permission, index: true, foreign_key: true
      t.boolean :active

      t.timestamps null: false
    end
  end
end
