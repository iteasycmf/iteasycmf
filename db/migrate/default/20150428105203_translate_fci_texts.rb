class TranslateFciTexts < ActiveRecord::Migration
  def self.up
    FciText.create_translation_table!({
                                          :value => :text, :shortvalue => :text
                                      }, {
                                          :migrate_data => true
                                      })
  end

  def self.down
    FciText.drop_translation_table! :migrate_data => true
  end
end
