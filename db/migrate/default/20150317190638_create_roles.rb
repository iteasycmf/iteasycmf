class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string :machine_name
      t.boolean :active
      t.boolean :blocked

      t.timestamps null: false
    end
  end
end
