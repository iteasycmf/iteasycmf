class AddFieldsTo < ActiveRecord::Migration
  def change
    add_column :menu_items, :mid, :integer
    add_column :menu_items, :model, :string
  end
end
