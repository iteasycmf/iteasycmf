class AddValueToDatabaze2 < ActiveRecord::Migration
  def up
    I18n.locale = :en
    MenuItem.create([{name: 'Forms', url: 'admin/transforms', menu_id: 1, public: true, position: 9, csscls: 'icon-note'}])
    System.create([{key: 'transforms', name: 'Form', machine_category: :record_type, value: 'Transform', blocked: true, hidden: true}])
  end
end
