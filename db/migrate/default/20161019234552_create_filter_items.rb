class CreateFilterItems < ActiveRecord::Migration[5.0]
  def up
    create_table :filter_items do |t|
      t.references :filter_group, foreign_key: true
      t.integer :mid
      t.string :model

      t.timestamps
    end
    FilterItem.create_translation_table! :value => :string
  end

  def down
    drop_table :filter_items
    FilterItem.drop_translation_table!
  end
end
