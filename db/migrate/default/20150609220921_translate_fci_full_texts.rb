class TranslateFciFullTexts < ActiveRecord::Migration
  def self.up
    FciFullText.create_translation_table!({
                                              :value => :text
                                          }, {
                                              :migrate_data => true
                                          })
  end

  def self.down
    FciFullText.drop_translation_table! :migrate_data => true
  end
end
