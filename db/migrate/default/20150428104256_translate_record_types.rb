class TranslateRecordTypes < ActiveRecord::Migration
  def self.up
    RecordType.create_translation_table!({
                                             :name => :string, :desc => :text, :help => :text, :label_title => :string,
                                             :url => :string
                                         }, {
                                             :migrate_data => true
                                         })
  end

  def self.down
    RecordType.drop_translation_table! :migrate_data => true
  end
end
