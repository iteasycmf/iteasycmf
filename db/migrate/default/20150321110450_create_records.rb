class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.references :record_type, index: true, foreign_key: true
      t.boolean :public
      t.boolean :sticky
      t.boolean :blocked
      t.references :seomore, index: true, foreign_key: true
      t.references :menu_item, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
