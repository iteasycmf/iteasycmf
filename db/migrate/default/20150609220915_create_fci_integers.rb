class CreateFciIntegers < ActiveRecord::Migration
  def change
    create_table :fci_integers do |t|
      t.integer :value
      t.references :fci, index: true, foreign_key: true
      t.integer :mid
      t.string :model

      t.timestamps null: false
    end
  end
end
