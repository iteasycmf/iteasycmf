class AddRecordTypeToModelRelation < ActiveRecord::Migration
  def change
    add_column :model_relations, :record_type, :string
  end
end
