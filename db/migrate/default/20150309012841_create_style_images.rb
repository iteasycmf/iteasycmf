class CreateStyleImages < ActiveRecord::Migration
  def change
    create_table :style_images do |t|
      t.string :name
      t.string :machine_name
      t.string :process
      t.integer :widht
      t.integer :height
      t.boolean :percent
      t.boolean :blocked

      t.timestamps null: false
    end
  end
end
