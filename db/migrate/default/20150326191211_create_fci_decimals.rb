class CreateFciDecimals < ActiveRecord::Migration
  def change
    create_table :fci_decimals do |t|
      t.decimal :value
      t.references :fci, index: true, foreign_key: true
      t.integer :mid
      t.string :model

      t.timestamps null: false
    end
  end
end
