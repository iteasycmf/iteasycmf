class AddValueTo < ActiveRecord::Migration
  def up

      I18n.locale = :en
      Block.create([{name: :content, region: :content, machine_name: :content, public: true, system: true, blocked: true, position: 0, entity: :system},
                    {name: :user_menu, region: :content, machine_name: :user_menu, public: true, system: true, blocked: true, position: -1, entity: :system}])


      I18n.locale = :en
      @seomore = Seomore.create!
      Record.create!(record_type_id: 1, public: true, blocked: true, url: :welcome, seomore_id: @seomore.id)
      Fci.create!([{name: 'Body', machine_name: :body, active: true, field_custom_id: 9, mid: 1, model: 'RecordType'}])
      FciString.create(fci_id: 1, mid: 1, model: 'Record', value: 'Welcome')
      FciFullText.create!(fci_id: 2, mid: 1, model: 'Record', value: 'Welcome!!!')


    if Menu.find_by(machine_name: :admin_menu).nil?
      I18n.locale = :en
      Menu.create!([{name: 'Admin menu', public: true, machine_name: :admin_menu, blocked: true},
                   {name: 'User menu', public: true, machine_name: :user_menu, blocked: true}])

      MenuItem.create!([{name: 'Dashboard', url: 'admin/dashboard', menu_id: 1, public: true, position: 0, csscls: 'icon-graph'},
                       {name: 'Type Records', url: 'admin/record_types', menu_id: 1, public: true, position: 4, csscls: 'icon-notebook'},
                       {name: 'Taxonomy', url: 'admin/taxonomies', menu_id: 1, public: true, position: 3, csscls: 'icon-tag'},
                       {name: 'Systems', url: 'admin/systems', menu_id: 1, public: true, position: 50, csscls: 'icon-settings'},
                       {name: 'Seo', url: 'admin/seomores', menu_id: 1, public: true, position: 49, csscls: 'icon-eye'},
                       {name: 'Menu', url: 'admin/menus', menu_id: 1, public: true, position: 1, csscls: 'icon-list'},
                       {name: 'Blocks', url: 'admin/blocks', menu_id: 1, public: true, position: 2, csscls: ' icon-grid'},
                       {name: 'Records', url: 'admin/records', menu_id: 1, public: true, position: 5, csscls: 'icon-pencil'},
                       {name: 'Users', url: 'admin/users', menu_id: 1, public: true, position: 6, csscls: 'icon-users'},
                       {name: 'Roles', url: 'admin/roles', menu_id: 1, public: true, position: 7, csscls: 'icon-ban'},
                       {name: 'Comments', url: 'admin/comments', menu_id: 1, public: true, position: 7, csscls: 'icon-bubbles'},
                       {name: 'Plugins', url: 'admin/roles', menu_id: 1, public: true, position: 7, csscls: 'icon-puzzle'}])

      MenuItem.create!([{name: 'Sign in', url: 'users/sign_in', menu_id: 2, public: true, position: 0},
                       {name: 'Sign up', url: 'users/sign_up', menu_id: 2, public: true, position: 1},
                       {name: 'Admin', url: 'admin/dashboard', menu_id: 2, public: true, position: 2},
                       {name: 'My Profile', url: 'users/edit', menu_id: 2, public: true, position: 3},
                       {name: 'Sign out', url: 'users/sign_out', menu_id: 2, public: true, position: 4}])
    end
  end
end
