class CreateMenuItems < ActiveRecord::Migration
  def change
    create_table :menu_items do |t|
      t.references :menu, index: true, foreign_key: true
      t.string :ancestry
      t.boolean :public
      t.integer :position
      t.string :csscls
      t.boolean :blocked

      t.timestamps null: false
    end
  end
end
