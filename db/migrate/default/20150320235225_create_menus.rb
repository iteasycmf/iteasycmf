class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.boolean :public
      t.references :block, index: true, foreign_key: true
      t.string :machine_name
      t.string :csscls
      t.boolean :blocked

      t.timestamps null: false
    end
  end
end
