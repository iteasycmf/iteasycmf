class CreateFciReferences < ActiveRecord::Migration
  def change
    create_table :fci_references do |t|
      t.references :fci, index: true, foreign_key: true
      t.integer :mid
      t.string :model
      t.string :rmodel
      t.integer :rrecord_type
      t.integer :rmid

      t.timestamps null: false
    end
  end
end
