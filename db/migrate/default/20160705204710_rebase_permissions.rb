class RebasePermissions < ActiveRecord::Migration
  def up
    Permission.all.map { |p| p.delete }

    %w( Record Profile User Block MenuItem Menu PermissionRole PluginManager
  	RecordType Role Seomore StyleImage System Taxonomy Comment ModelRelation 
  	ModelSetting Cell Fci Transform).each do |subject|
      %w( create read update destroy ).each do |action|
        Permission.create(subject_class: subject, action: action, admin: true, name: action+" to "+subject)
      end
    end
    %w( Record Profile User Taxonomy Comment Cell Transform).each do |subject|
      %w( create read update destroy ).each do |action|
        Permission.create(subject_class: subject, action: action, admin: false, name: action+" to "+subject)
      end
    end
    %w( MenuItem Taxonomy Block Fci ).each do |subject|
      %w( sort ).each do |action|
        Permission.create(subject_class: subject, action: action, admin: true, name: "Edit sort to "+subject)
      end
    end

    Permission.create([{subject_class: 'RecordType', action: :fields, admin: true, name: "Add fields to record type"}])
    Permission.create(subject_class: 'Record', action: :taxonomypage, admin: true, name: "Add taxonomy to record")
    Permission.create(subject_class: 'Record', action: :relationpage, admin: true, name: "Add relation to record")
    Permission.create(subject_class: 'Taxonomy', action: :relationpage, admin: true, name: "Add relation to taxonomy")
    Permission.create(subject_class: 'Block', action: :relationpage, admin: true, name: "Add relation to block")
    Permission.create(subject_class: 'Profile', action: :relationpage, admin: true, name: "Add relation to profile")

  end
end
