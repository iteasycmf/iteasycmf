class CreateModelRelations < ActiveRecord::Migration
  def change
    create_table :model_relations do |t|
      t.integer :mid_main
      t.string :model_main
      t.integer :mid
      t.string :model

      t.timestamps null: false
    end
  end
end
