class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :user, index: true, foreign_key: true
      t.references :record_type, index: true, foreign_key: true
      t.integer :mid
      t.string :model
      t.string :ancestry

      t.timestamps null: false
    end
  end
end
