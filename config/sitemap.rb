host root_url

sitemap :site do
  url root_url, last_mod: Time.now, change_freq: "daily", priority: 1.0
  url records_url
  Record.all.each do |post|
    url post
  end
end

ping_with "http://#{host}/sitemap.xml"
