Rails.application.routes.draw do
  namespace :admin do
    resources :messages
  end
  resources :messages
  filter :pagination, :uuid
  filter :locale
  get 'switch_user', to: 'switch_user#set_current_user'
  get 'switch_user/remember_user', to: 'switch_user#remember_user'
  mount Ckeditor::Engine => '/ckeditor'
  mount ActionCable.server => '/cable'
  devise_for :users, :controllers => {sessions: 'sessions', registrations: 'registrations'}
  root :to => 'records#show', id: '1'
  get 'sheet/search'
  get 'sitemap' => 'sheet#sitemap', format: :xml, as: :sitemap
  get 'robots' => 'sheet#robots', format: :text
  get 'feeds', to: 'sheet#feeds', format: 'rss'
  match '/404', to: 'errors#not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all
  get 'admin/record_types/:id/fields' => 'admin/record_types#fields', as: 'admin_record_type_fields'
  get 'admin/records/:id/taxonomypage' => 'admin/records#taxonomypage', as: 'admin_records_taxonomypage'
  get 'admin/records/:id/relationpage' => 'admin/records#relationpage', as: 'admin_records_relationpage'
  get 'admin/blocks/:id/relationpage' => 'admin/blocks#relationpage', as: 'admin_blocks_relationpage'
  get 'admin/taxonomies/list/:record_type_id' => 'admin/taxonomies#list', as: 'admin_taxonomies_list'
  get 'admin/records/list/:record_type_id' => 'admin/records#list', as: 'admin_records_list'
  get 'admin/comments/list/:record_type_id' => 'admin/comments#list', as: 'admin_comments_list'
  get 'admin/transforms/list/:record_type_id' => 'admin/transforms#list', as: 'admin_transforms_list'
  get 'admin/dashboard' => 'admin/statics#dashboard', as: 'admin_dashboard'
  get 'welcome' => 'records/1'
  resources :chat_rooms, only: [:new, :create, :show, :index]

  resources :users, except: [:index]
  resources :profiles
  resources :transforms, only: [:create, :show]
  resources :comments
  resources :flags, only: [:create, :destroy, :index]
  resources :cells
 # resources :records
  resources :users do
    post :unlock, :on => :member
  end

  namespace :admin do
    resources :model_relations
    resources :plugin_managers
    resources :style_images
    resources :fci_dates
    resources :fci_images
    resources :fci_booleans
    resources :fci_decimals
    resources :fci_texts
    resources :fci_strings
    resources :fci_references
    resources :fci_full_texts
    resources :fci_files
    resources :fci_integers
    resources :fci_ratings
    resources :fcis do
      put :sort, on: :collection
    end
    resources :field_customs
    resources :records do
      resources :seomores
    end
    resources :record_types
    resources :taxonomies do
      post :sort, on: :collection
    end
    resources :systems
    resources :seomores
    resources :roles
    resources :users
    resources :menu_items do
      post :sort, on: :collection
    end
    resources :menus
    resources :blocks do
      post :sort, on: :collection
    end
    resources :permissions
    resources :profiles
    resources :cells
    resources :comments
    resources :transforms
    resources :users_roles
    resources :filter_groups
    resources :flags

  end
  #end
  DynamicRouter.load
  #get '*path', to: redirect("/#{I18n.locale}/%{path}"), constraints: lambda { |req| !req.path.starts_with? "/#{I18n.locale}/" }
  #get '', to: redirect("/#{I18n.locale}")


end
