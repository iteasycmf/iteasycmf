require_relative 'boot'

require 'rails/all'
require 'csv'
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Iteasycmf
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.exceptions_app = self.routes
    #config.active_job.queue_adapter = :delayed_job
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
    config.i18n.fallbacks = [:uk, :ru, :en]
    config.i18n.available_locales = [:uk, :ru, :en]
    config.i18n.default_locale = :en
    config.time_zone = 'Europe/Kiev'
    config.assets.precompile += %w(*.png *.jpg *.jpeg *.gif *.odt codemirror* codemirror/**/*)
    config.assets.enabled = true
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)
    config.assets.precompile += Ckeditor.assets
    config.assets.precompile += %w( ckeditor/* )
    config.assets.precompile += %w(*.png *.jpg *.jpeg *.gif)
    initializer 'setup_asset_pipeline', :group => :all do |app|
      # We don't want the default of everything that isn't js or css, because it pulls too many things in
      app.config.assets.precompile.shift

      # Explicitly register the extensions we are interested in compiling
      app.config.assets.precompile.push(Proc.new do |path|
        File.extname(path).in? [
                                   '.html', '.erb', '.haml', # Templates
                                   '.png', '.gif', '.jpg', '.jpeg', # Images
                                   '.eot', '.otf', '.svc', '.woff', '.ttf', # Fonts
                               ]
      end)
    end
  end
end
